# Copyright (C) 2020-2021 Alex Codoreanu
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import matplotlib.gridspec as gridspec
from matplotlib import pyplot as plt
from matplotlib import use as m_use
from numpy import arange, ceil

from base import label_font, title_font, xtick_style, ytick_style
from base import marker_style, figure_style, background_color

m_use('Agg')


def heatmap_plot(data,
                 x,
                 y,
                 z,
                 plot_name=None,
                 plot_title=None,
                 color_map=None):
    """ This is a generic heatmap plot generator.

    USAGE:
        from pandas import read_csv
        from plot_utils.create import heatmap_plot

        test_path = '/fred/oz996/PyCharm/alexc/spiir_testing/plot_utils_dev/run_plots_dev'
        data = read_csv('{}/dataframes/df_latency_30.csv'.format(test_path))
        x = 'detected_snr_H'
        y = 'detected_snr_L'
        z = 'detected_snr_V'
        plot_title = 'heatmap_test.png'
        plot_name = plot_title
        color_map = 'YlGnBu'

        heatmap_plot(data, x, y, z, plot_title, plot_name, color_map=color_map)

    :param data: A dataframe object containing columns x, y, z
    :param x: column name for x-axis data
    :param y: column name for y-axis data
    :param z: column name for z-axis data
    :param plot_title: string denoting the plot title
    :param plot_name: string denoting the output name for the saved object
    :param color_map: string colormap identifier
    
    :return:
    """
    if plot_name is None:
        print 'You did not pass me a plot name to save to.'
        exit(1)

    if 'dataframe' not in str(type(data)).lower():
        print 'data has to be a DataFrame'
        exit(1)

    if len({x, y, z}.intersection(set(data.keys()))) != 3:
        print "One or more of the keys passed are not in the dataframe"
        exit(1)

    data = data.round(1)
    plt.figure(**figure_style)

    table = data.pivot_table(values=z, index=y, columns=x)

    if color_map is None:
        heatmap(table, annot=True, cbar_kws={'label': z})
    else:
        heatmap(table, annot=True, cbar_kws={
            'label': z
        }, cmap=color_map).invert_yaxis()

    if plot_title is not None:
        plt.title(plot_title, **title_font)

    try:
        plt.savefig(plot_name)
    except OSError:
        print "Could not save to specified directory.\n" \
              "Please check that you have write permissions."
        exit(1)

    plt.close('all')


def scatter_plot_2hist(data,
                       x_variables=None,
                       y_variables=None,
                       label_variables=None,
                       output_file_name=None,
                       plot_title=None,
                       ylabel=None,
                       xlabel=None,
                       min_x=None,
                       max_x=None,
                       min_y=None,
                       max_y=None,
                       max_x_counts=None,
                       max_y_counts=None,
                       xlog=False,
                       ylog=False,
                       xbinwidth=0.05,
                       ybinwidth=0.1,
                       grid=True,
                       verbose=False):
    """This is a generic function that creates a combination scatter and associated
    histogram for each variable. It takes in a DataFrame that holds multiple x/y columns.
    These columns (keys) need to be identified and passed as list objects, even if it's
    a list with a single item.

    Adapted from code provided by Dr. Fiona Panther.

    USAGE:
        from pandas import read_csv
        from plot_utils.create import scatter_plot_2hist

        test_path = '/fred/oz996/PyCharm/alexc/spiir_testing/plot_utils_dev'
        data = read_csv('{}/postcoh__summary.csv'.format(test_path))

        output_file_name = 'test.png'
        snr_variables = ['snglsnr_L1', 'snglsnr_H1', 'snglsnr_V1']
        chi2_variables = ['chisq_L1', 'chisq_H1', 'chisq_V1']
        label_variables = ['LIGO-Livingston', 'LIGO-Hanford', 'LIGO-Virgo']

        scatter_plot_2hist(data, x_variables=snr_variables, y_variables=chi2_variables, label_variables=label_variables,
                           output_file_name='test.png', plot_title='My test scatter plot', ylabel="Chi$^2$",
                           xlabel="SNR", verbose=True, grid=True, min_x=4, max_x=50, min_y=1, max_y=100, xlog=True,
                           ylog=True, max_y_counts=100, max_x_counts=200)

    :param data: REQUIRED DataFrame object with keys containing the x/y _variables
    :param x_variables: REQUIRED list of DataFrame column names that will be the x-axis data
    :param y_variables: REQUIRED list of DataFrame column names that will be the y-axis data
    :param label_variables: REQUIRED list of labels for the plot legend
    :param output_file_name: OPTIONAL string of output name
    :param plot_title: OPTIONAL string of plot title
    :param ylabel: OPTIONAL string of y-axis label
    :param xlabel: OPTIONAL string of x-axis label
    :param min_x: OPTIONAL numerical value for min x value
    :param max_x: OPTIONAL numerical value for max x value
    :param min_y: OPTIONAL numerical value for min y value
    :param max_y: OPTIONAL numerical value for max y value
    :param xlog: OPTIONAL bool which turns x-axis to log scale
    :param ylog: OPTIONAL bool which turns y-axis to log scale
    :param max_x_counts: OPTIONAL numerical value for max x value histogram
    :param max_y_counts: OPTIONAL numerical value for max y value histogram
    :param xbinwidth: OPTIONAL numerical value for x-axis bin value for histogram plot
                      DEFAULT value is set 0.05.
    :param ybinwidth: OPTIONAL numerical value for y-axis bin value for histogram plot
                      DEFAULT value is set 0.1.
    :param grid: OPTIONAL bool to
    :param verbose: OPTIONAL
    :return:

    """

    if 'dataframe' not in str(type(data)).lower():
        print "'data' variable has to be of type 'DataFrame'"
        exit(0)

    for var in [x_variables, y_variables]:
        if var is None:
            print "You must tell me the x/y columns you want to plot"
            exit(0)

    if verbose:
        print "\nx-columns are: {}".format(x_variables)
        print "y-columns are: {}".format(y_variables)
        print "labels are: {}".format(label_variables)

    if label_variables is None:
        label_variables = list()
        for _ in list(range(len(x_variables))):
            label_variables.append('')

    # check that x_variables, y_variables, label_variables
    # have the same number of elements
    if len(x_variables) != len(y_variables) != len(label_variables):
        print 'The x_variables & y_variables\n' \
              'NEED to have the same number of elements'
        exit(1)

    if max_x is None:
        max_x = 0.0
        for var_index in list(range(len(x_variables))):
            max_x = max([data[x_variables[var_index]].max()], max_x)

        max_x = max_x[0]

    if max_y is None:
        max_y = 0.0
        for var_index in list(range(len(x_variables))):
            max_y = max([data[y_variables[var_index]].max()], max_y)

        max_y = max_y[0]

    if min_x is None:
        min_x = max_x
        for var_index in list(range(len(x_variables))):
            min_x = min([data[x_variables[var_index]].min()], min_x)

        min_x = min_x[0]

    if min_y is None:
        min_y = max_y
        for var_index in list(range(len(x_variables))):
            min_y = min([data[y_variables[var_index]].min()], min_y)

        min_y = min_y[0]

    if verbose:
        print "x-axis limits are:[{},{}]".format(min_x, max_x)
        print "y-axis limits are:[{},{}]".format(min_y, max_y)

    xbins = arange(min_x, ceil(max_x) + ybinwidth, ybinwidth)
    ybins = arange(min_y, ceil(max_y) + xbinwidth, xbinwidth)

    plt.figure(**figure_style)

    gs = gridspec.GridSpec(2, 2, width_ratios=[2, 1], height_ratios=[2, 1])

    gs.update(wspace=0.025, hspace=0.025)

    ax1 = plt.subplot(gs[0])
    for var_index in list(range(len(x_variables))):
        ax1.scatter(data[x_variables[var_index]],
                    data[y_variables[var_index]],
                    s=10,
                    alpha=0.5,
                    label=label_variables[var_index])

    ax1.set_xlim([min_x, max_x])
    ax1.set_ylim([min_y, max_y])
    if ylabel is not None:
        ax1.set_ylabel(ylabel, **title_font)

    ax1.xaxis.set_ticklabels([])
    if grid:
        plt.grid(True)

    if plot_title is not None:
        plt.title(plot_title, **title_font)

    plt.xticks(**xtick_style)
    plt.yticks(**ytick_style)
    if xlog:
        plt.xscale('log')

    if ylog:
        plt.yscale('log')

    ax2 = plt.subplot(gs[1])
    for var_index in list(range(len(x_variables))):
        ax2.hist(data[y_variables[var_index]],
                 bins=ybins,
                 histtype='step',
                 lw=4,
                 orientation='horizontal',
                 label=label_variables[var_index])

    ax2.set_xlabel('Counts', **title_font)
    if max_y_counts is not None:
        ax2.set_xlim([0, max_y_counts])

    ax2.set_ylim([min_y, max_y])
    ax2.yaxis.set_ticklabels([])
    if grid:
        plt.grid(True)

    if ylog:
        plt.yscale('log')

    plt.xticks(**xtick_style)

    ax3 = plt.subplot(gs[2])
    for var_index in list(range(len(x_variables))):
        ax3.hist(data[y_variables[var_index]],
                 bins=xbins,
                 histtype='step',
                 lw=4,
                 label=label_variables[var_index])

    ax3.set_ylabel('Counts', **title_font)
    if xlabel is not None:
        ax3.set_xlabel(xlabel, **title_font)

    if max_x_counts is not None:
        ax3.set_ylim([0, max_x_counts])

    ax3.set_xlim([min_x, max_x])

    if xlog:
        plt.xscale('log')

    plt.xticks(**xtick_style)
    plt.yticks(**ytick_style)

    ax3.legend(loc='upper right',
               prop={'size': 14},
               scatterpoints=1,
               bbox_to_anchor=(1.55, 0.7),
               frameon=True,
               fancybox=True,
               shadow=True)

    if grid:
        plt.grid(True)

    if verbose:
        print "\nsaved image to:\n{}".format(output_file_name)

    try:
        plt.savefig(output_file_name)
    except OSError:
        print "Could not save the output image to:\n{}\n" \
              "Please check that you have write permissions " \
              "in the respective output directory".format(output_file_name)


def scatter_plot(x_data,
                 y_data,
                 plot_name,
                 data_labels=None,
                 xlabel=None,
                 ylabel=None,
                 xlog=False,
                 ylog=False,
                 background=True,
                 grid=True,
                 title=True,
                 verbose=True,
                 custom_style=None,
                 custom_marker_style=None):
    """This is a generic scatter plot function which
     has access to preloaded plot style parameters from base.py.

    :param x_data: x_data vector
    :param y_data: y_data vector
    :param plot_name: the full name with path of the plot to be saved
    :param xlabel: xlabel string
    :param ylabel: ylabel string
    :param xlog: if 'True' then axis will be in log-scale
    :param ylog: if 'True' then axis will be in log-scale
    :param background: if 'True' then background will be base.background_color
    :param grid: if 'True' then grid lines will be displayed
    :param title: if 'True' then a title will be extracted from plot_name
                  from the last '/' to the last '.' character.
    :param verbose: if 'True' then print statements will be displayed
    :return:
    """

    if len(x_data) != len(y_data):
        print('Number of elements do not match in x/y data variables\n')
        return 1

    if custom_style is not None:
        figure_style = custom_style

    plt.figure(**figure_style)

    if custom_marker_style is not None:
        marker_style = custom_marker_style

    if background:
        plt.gca().set_facecolor(background_color)

    try:
        if 'list' in str(type(x_data)):
            for i in list(range(x_data)):
                x_data_local = x_data[i]
                y_data_loccal = y_data[i]
                if data_labels is not None:
                    label = data_labels[i]
                    plt.plot(x_data_local,
                             y_data_loccal,
                             '-',
                             label=label,
                             linewidth=2)

            print(x_data)

        if data_labels is not None:
            plt.legend()

        else:
            plt.plot(x_data, y_data, '-', **marker_style)

    except Exception as exception_message:
        print "FAILED with EXCEPTION:\n{}".format(exception_message)

    plt.xticks(**xtick_style)
    plt.yticks(**ytick_style)

    if xlog:
        plt.xscale('log')

    if ylog:
        plt.yscale('log')

    if xlabel is not None:
        try:
            xlabel = str(xlabel)
            plt.xlabel(xlabel, **label_font)
        except TypeError:
            print "Label string must be or be able to be cast to a type(str) variable."

    if ylabel is not None:
        try:
            ylabel = str(ylabel)
            plt.ylabel(ylabel, **label_font)
        except TypeError:
            print "Label string must be or be able to be cast to a type(str) variable."

    if grid:
        plt.grid(True)

    if title:
        plot_title = plot_name[plot_name.rfind('/') + 1:
                               plot_name.find('.')].replace('_', '$\_$') \
                     + '\n'
        plt.title(plot_title, **title_font)

    try:
        plt.savefig(plot_name)
    except IOError:
        print 'Could not save plot:\n{}\n\n'.format(plot_name)
        print 'Check that you have write permission in directory.\n\n'

    plt.close('all')
    if verbose:
        print(plot_name)
