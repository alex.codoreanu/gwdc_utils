# Copyright (C) 2020-2021 Alex Codoreanu
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import gc
import glob
import re
from os.path import isfile
from sys import exit as goodbye

from gstlal.spiirbank.cbc_template_iir import Bank
from numpy import real
from numpy import where
from pandas import read_csv
from postprocess_utils.create import module_path

current_IFO_short_names = ['L1', 'V1', 'H1']
current_latency_values = [0, 10, 20, 30]

bank_utils_path = module_path.replace('postprocess_utils', 'bank_utils')


def get_approximant(bank_id):
    """ This function returns the appropriate approximant based on the bank number.
    The logic it implements is:
        if bank_id < 90 then approximant = 'SpinTaylorT4'
        if bank_id > 89 && bank_id < 100 then approximant = 'SEOBNRv4_ROM'
        if bank id > 99 && bank_id < 112 then approximant = 'SpinTaylorT4'
        if bank_id > 111 then approximant = 'SEOBNRv4_ROM'

    :param bank_id: numerical value that can be cast to int
    :return approximant: string name of approximant file
    """

    try:
        bank_id = int(bank_id)
    except (TypeError, ValueError):
        print "You've passed me this value for bank_id: {}\n" \
              "and I can NOT cast this to an int.\n" \
              "ERROR: get_approximant()".format(bank_id)
        goodbye(0)

    approximant = 'SEOBNRv4_ROM'
    if 99 < bank_id < 112:
        approximant = 'SpinTaylorT4'
    if bank_id < 90:
        approximant = 'SpinTaylorT4'

    return approximant


def get_num_jobs(num_directories, dir_path, num_jobs=4):
    """This function identifies the number of individual threads that will be required based on:
        - the number of identified directories
        - the number of job-threads available:
            - default is set to 4 as that is the polite limit available on the ozstar login nodes

    :param dir_path: the directory path to the location of the templates and logs
    :param num_directories: the number of logs_* directories in the dir_path
    :param num_jobs: the default number of jobs is set to 4. If you'd like to run more threads, make sure that
                      you make an appropriate request.
    :return: int(num_jobs)
    """
    if num_directories < num_jobs:
        num_jobs = num_directories

    if num_jobs < 1:
        print "ERR get_num_jobs:\nI couldn't find any 'logs_*' directories " \
              "in the directory path you passed:{}".format(dir_path)
        goodbye(0)

    return int(num_jobs)


def extract_job_parameters_from_directory_name(log_directory, dir_path):
    """This function will extract the job submission parameters from the output directory name.
    The variable names correspond 1to1 with the variable names used in the job submission scripts.
    These variables are:

    BOPTERC
    SBANK
    NEGLAT
    SUF

    :param log_directory: the identified log directory
    :param dir_path: the path to the identified log directory. This is the location of the template files
    :return: BOPTPERC, SBANK, NEGLAT, SUF
    """
    try:
        SUF = log_directory[log_directory.find('logs_b0') + 7:].replace(
            'optmin', '')
        BOPTPERC = SUF[:2]
        SUF = SUF.replace('{}pc_'.format(BOPTPERC), '')
        SBANK, NEGLAT = SUF.split('_')

        return BOPTPERC, SBANK, NEGLAT, SUF
    except (RuntimeError, TypeError, NameError):
        print "ERR extract_job_parameters_from_directory_name:\n" \
              " I couldn't extract job parameters from the directory" \
              " path you passed:{}".format(dir_path)
        goodbye(0)


def write_header_for_error_status_file(log_directory,
                                       overwrite=False,
                                       header_line=None):
    """This function will take in the log directory and create a status file to where all of the
    relevant lines will be appended to.

    :param log_directory: the relevant log directory
    :param overwrite: a BOOL which will define if an existing file should be overwritten (ie. the function has already
    been run in the past but maybe failed for some reason)
    :param header_line: the default header line. This should only be changed if the submission scripts change.

    :return: output_file: the name of the *_err_status.csv file
    """
    # set default header line
    if header_line is None:
        header_line = 'BOPTPERC,SBANK,NEGLAT,job_id,IFO,BANK,template_number,' \
                      'm1_mass,m2_mass,epsilon_final,final_filters,match_final,' \
                      'epsilon_original,original_filters,match_original\n'
    try:
        output_file = '{}_err_status.csv'.format(log_directory)

        # check if the output file exists
        if isfile(output_file):
            print '{} already exists'.format(output_file)
            if overwrite:
                with open(output_file, 'w') as w:
                    w.writelines(header_line)
                print 'and now has been overwritten like you told me to'
            else:
                print 'and I did not overwrite it'
        else:
            with open(output_file, 'w') as w:
                w.writelines(header_line)

            print "I've written the header for file:\n{}".format(output_file)

        return output_file
    except (RuntimeError, TypeError, NameError):
        print 'ERR write_header_for_error_status_file:\n'
        goodbye(0)


def parse_relevant_err_file_line(line,
                                 job_id,
                                 IFO,
                                 BANK,
                                 BOPTERC,
                                 SBANK,
                                 NEGLAT,
                                 err_out=True):
    """This function parses a passed line and identifies the relevant variables and puts them together in the same order
    as passed in the header line.

    :param line: input line to be parsed
    :param job_id: SLURM job id

    THE BELOW ARE JOB SUBMISSION RELEVANT SCRIPTS THAT HAVE BEEN PULLED LOWER DOWN THE CALL TREE.
    :param IFO:
    :param BANK:
    :param BOPTERC:
    :param SBANK:
    :param NEGLAT:

    :param err_out: a BOOL value that decides whether or not to fail in the case of a parse error.

    :return: output line: comma separated line with the same order of items as passed to the header
    """
    first_check = False
    try:
        line_suffix = line[line.find('template'):]
        line_suffix = line_suffix.replace('\n', '')
        line_suffix = line_suffix.replace('match.', 'match,')
        template_string, \
        masses, \
        epsilon_final, \
        match_final, \
        original_epsilon, original_match = line_suffix.split(',')
        first_check = True
    except (RuntimeError, TypeError, NameError):
        print "ERR parse_relevant_err_file_line:"
        print "did not successfully parse line into parameter relevant substrings:"
        print line
        if err_out:
            goodbye(0)
        else:
            return ''
    if first_check:
        try:
            # get the template id
            template_id = template_string.replace('template', '')
            template_id = template_id[:template_id.find('/')]
            template_id = template_id.strip()
            # get the m1, m2 masses
            for el in ['m1', 'm2', '=']:
                masses = masses.replace(el, '')

            m1_mass, m2_mass = masses.split()

            # get final epsilon parameters
            epsilon_final_string, final_filters_string = epsilon_final.split(
                ':')
            epsilon_final_string = epsilon_final_string.replace(
                'epsilon = ', '').strip()
            final_filters_string = final_filters_string.replace('filters',
                                                                '').strip()
            match_final_string = match_final.replace('match', '').strip()

            # get original epsilon parameters
            epsilon_original_string, original_filters_string = original_epsilon.split(
                ':')
            epsilon_original_string = epsilon_original_string.replace(
                'original_eps =', '').strip()
            original_filters_string = original_filters_string.replace(
                'filters', '').strip()
            match_original_string = original_match.replace('match', '').strip()

            output_line = ','.join(
                list([
                    BOPTERC, SBANK, NEGLAT, job_id, IFO, BANK, template_id,
                    m1_mass, m2_mass, epsilon_final_string,
                    final_filters_string, match_final_string,
                    epsilon_original_string, original_filters_string,
                    match_original_string
                ]))
            output_line = '{}\n'.format(output_line)

            return output_line

        except (RuntimeError, TypeError, NameError):
            print "ERR parse_relevant_err_file_line:"
            print "did not succesfully extract variables from sub_strings for line:"
            print line
            if err_out:
                goodbye(0)
            else:
                return ''


def create_err_status_file(log_directory,
                           dir_path,
                           overwrite_original=True,
                           err_out_lines=False):
    # type: (object, object, object, object) -> object
    """This function creates a single status file associated with each .err file in the log directory in dir_path.
    This file holds all of the relevant single line entries from the .err files (ie. SBANK, NEGLAT, m1, m2, ...)

    @param log_directory:
    @param dir_path:
    @param overwrite_original:
    @param err_out_lines:

    @return: 0
    """
    BOPTPERC, \
    SBANK, \
    NEGLAT, \
    SUF = extract_job_parameters_from_directory_name(log_directory, dir_path)

    # get all .err files in log_directory
    err_file_list = glob.glob('{}/*err'.format(log_directory))

    # write the header for the err_status file
    output_file = write_header_for_error_status_file(
        log_directory, overwrite=overwrite_original)

    for err_file_name in err_file_list:
        # extract job parameters from err_file_name
        err_file_prefix = err_file_name[err_file_name.find('/gstlal') + 1:]
        err_file_prefix = err_file_prefix.replace('gstlal_iir_bank_', '')
        err_file_prefix = err_file_prefix.replace('.err', '')

        job_id, IFO, BANK = err_file_prefix.split('_')

        for line in open(err_file_name).xreadlines():
            if 'template' in line:
                if '/1000' in line:
                    output_line = parse_relevant_err_file_line(
                        line,
                        job_id,
                        IFO,
                        BANK,
                        BOPTPERC,
                        SBANK,
                        NEGLAT,
                        err_out=err_out_lines)
                    if len(output_line) > 2:
                        with open(output_file, 'a') as a:
                            a.writelines(output_line)


def create_single_bank_report_file(
        dir_path,
        report_file_string='logs_*',
        report_file_type='csv',
        match_final_threshold=0.9,
        output_file_id='combined_data_report_for_all_logs.csv',
        stats_file_id='template_bank_report_default.csv',
        bad_match_final_data_output_id='templates_to_be_rerun.csv'):
    """This function combines all the single outputs from create_err_status_file into a single file.

    usage:
        from bank_utils.base import create_single_bank_report_file
        create_single_bank_report_file('/fred/oz016/PSD-1263344418-21600')
    @param dir_path:
    @param report_file_string:
    @param report_file_type:
    @param match_final_threshold:
    @param output_file_id:
    @param stats_file_id:
    @param bad_match_final_data_output_id:

    @return: 0
    """
    output_file_name = '{}/{}'.format(dir_path, output_file_id)
    stats_file_name = '{}/{}'.format(dir_path, stats_file_id)
    bad_match_final_data_output = '{}/{}'.format(
        dir_path, bad_match_final_data_output_id)

    report_file_check = '{}/{}csv'.format(dir_path, report_file_string,
                                          report_file_type)

    report_files = glob.glob(report_file_check)
    print 'In directory:{}\n' \
          'I found the following bank report files:'.format(dir_path)

    for report_file in report_files:
        print str(report_file).replace(dir_path, '')

    data = read_csv(report_files[0])
    if len(report_files) > 1:
        for report_file in report_files[1:]:
            temp_data = read_csv(report_file)
            data = data.append(temp_data)
            del temp_data

    data.to_csv(output_file_name, index=False)
    print 'All individual reports have now been appended\n' \
          'and written to file:\n{}'.format(output_file_name)

    gc.collect()

    # print how many templates there are with a value of below value of
    # match_final_threshold -- default is set to 0.9
    templates_to_be_rerun = data.loc[data.match_final < match_final_threshold]
    templates_to_be_rerun.to_csv(bad_match_final_data_output, index=False)

    print "\n\nThere are {} templates below " \
          "the threshold value: {}".format(templates_to_be_rerun.NEGLAT.size,
                                           match_final_threshold)

    print "These templates have been written to file:\n{}".format(
        bad_match_final_data_output)

    sorted_latencies = templates_to_be_rerun.NEGLAT.unique().tolist()
    sorted_latencies.sort()

    try:
        print "FOR NO LATENCY files: there are {} templates with " \
              "match_final values below " \
              "{}".format(templates_to_be_rerun.loc[templates_to_be_rerun.NEGLAT == 0].NEGLAT.size,
                          match_final_threshold)
    except ValueError:
        print '\n'

    del templates_to_be_rerun
    gc.collect()

    sorted_latencies = data.NEGLAT.unique().tolist()
    sorted_latencies.sort()

    header_line = 'NEGLAT,IFO,num_banks\n'
    with open(stats_file_name, 'w') as w:
        w.writelines(header_line)

    for latency in sorted_latencies:
        subset = data.loc[data.NEGLAT == latency]
        for IFO in subset.IFO.unique().tolist():
            IFO_subset = subset.loc[subset.IFO == IFO]
            print "latency: {},instrument: {},number of banks:{}".format(
                latency, IFO,
                IFO_subset.BANK.unique().size)
            with open(stats_file_name, 'a') as a:
                stats_line = '{},{},{}\n'.format(latency, IFO,
                                                 IFO_subset.BANK.unique().size)
                a.writelines(stats_line)
        del subset
        gc.collect()

    return 0


def get_bank_object(bank_name=None):
    """This function returns a bank object.
    
    @param bank_name: name with full path of the bank to be read in

    @return: bank object
    """
    if bank_name is None:
        print("You failed to pass me bank name variable.")
        exit(0)
    else:
        try:
            tmp_bank = Bank()  # type: Bank
            tmp_bank.read_from_xml(bank_name)
            return tmp_bank

        except Exception as local_exception:
            print("Failed to read in bank object.\nException: {}".format(
                local_exception))
            exit(0)


def get_common_rate(reference_bank, comparison_bank):
    try:
        reference_rates = reference_bank.get_rates()
        comparison_rates = comparison_bank.get_rates()
    except:
        print("\n\nUnable to extract rates from one of the banks\n")
        goodbye(1)

    return list(set(reference_rates).intersection(comparison_rates))


def get_single_template_parameters_from_bank_object(bank_object,
                                                    rate=None,
                                                    template_id=None):
    """ This function extracts the real components of the A, B, and D components
    of a template(template_id) for a given rate from a bank_object.

    @param bank_object:
    @param tmplt_id:
    @return: realA, realB, realD
    """

    if rate == None or template_id == None:
        print("\n\nYou must pass a value to: rate or template_id\n")
        goodbye(1)

    realA = real(bank_object.A[rate][template_id])
    realB = real(bank_object.B[rate][template_id])
    realD = real(bank_object.D[rate][template_id])

    return realA, realB, realD


def compare_template_parameters(reference_items=None,
                                comparison_items=None,
                                reference_names=None,
                                pct_theshold=None):
    """ This function computes the relative difference between each sequncial and respective elements of
    reference_items and comparison_items.

    Returns TRUE if relative difference is below or equal to pct_theshold.

    @param reference_items: list of lists containing baseline elements
    @param comparison_items: list of lists containing comparison elements
    @param reference_names: list of strings identifiers corresponding to baseline/comparison elements
    @param pct_theshold: percentage threshold to be applied to each item comparison. Default value is 5%

    @return: check_bool: True if relative difference for each item is below pct_threshold
    """

    check_bool = True

    if pct_theshold is None:
        print("Using default threshold of 5%\n")
        pct_theshold = 5.0

    variable_names = ['reference_items', 'comparison_items', 'reference_names']
    for local_index, local_item in enumerate(
        [reference_items, comparison_items, reference_names]):
        if local_item is None:
            print("You did not pass me any data for: {}".format(
                variable_names[local_item]))
            goodbye(1)

    del variable_names, local_index, local_item

    average_value = len(reference_items) + \
                    len(comparison_items) + \
                    len(reference_names)

    if float(average_value) / len(current_IFO_short_names) != len(
            current_IFO_short_names):
        print("reference_items, comparison_items, reference_names "
              "need to be list objects "
              "with the same length")
        goodbye(1)

    for local_index, reference_item in enumerate(reference_items):
        print('Checking {}\n'.format(reference_names[local_index]))
        min_len = min(
            [len(reference_item),
             len(comparison_items[local_index])])
        if min_len > 1:
            max_difference = max(
                abs((comparison_items[local_index][:min_len - 1] -
                     reference_item[:min_len - 1])))
        else:
            print("The minimum length of one of the {} "
                  "variables is not greater than 1".format(
                      reference_names[local_index]))

            return False

        print('Maximum difference is: {}\n'.format(max_difference))

        # find the index where max_difference occurs and
        # divide by the relevant reference_item
        if float(max_difference) != 0.0:
            difference = abs((comparison_items[local_index][:min_len - 1] -
                              reference_item[:min_len - 1]))
            max_location = where(difference == max_difference)

            for local_location in max_location:
                pct_difference = abs(
                    comparison_items[local_index][local_location[0]] -
                    reference_item[local_location[0]])
                pct_difference = pct_difference / abs(
                    reference_item[local_location[0]])

                if pct_difference * 100.0 > pct_theshold:
                    print(
                        "Percentage difference {} is above current threshold "
                        "of {}".format(pct_difference * 100.0, pct_theshold))
                    check_bool = False

    return check_bool


def get_reference_bank_object(IFO=None, negative_latency=None):
    """This function loads a reference bank object for the bank generation unit test.

    @param IFO: instrument short name such as a single element from: ['L1', 'V1', 'H1']
    @param negative_latency: latency value to be used.
                             Current options: [0, 10, 20, 30]
    @return: reference bank object
    """

    for item in [IFO, negative_latency]:
        if item is None:
            print("You did not pass me a value for: {}".format(item))
            goodbye(1)

    if IFO not in current_IFO_short_names:
        print("You passed me IFO:{} but it's not in current_IFO_short_names".
              format(IFO))
        goodbye(1)

    negative_latency = int(negative_latency)
    reference_bank_name = "{}/reference_banks/neg_lat_{}/" \
                              "{}-GSTLAL_SPLIT_BANK_0003-0-0.xml.gz".format(bank_utils_path,
                                                                            negative_latency,
                                                                            IFO)
    if not isfile(reference_bank_name):
        print("Tried to load a bank for IFO:{} with negative_latency = {}\n".
              format(IFO, negative_latency))
        print("{} does not exist, you might need to create that bank".format(
            reference_bank_name))

    print("\nReading in the following reference bank:\n{}".format(
        reference_bank_name))
    return get_bank_object(bank_name=reference_bank_name)


def make_dictionary(state_file_name=None):
    """This function is designed to create a multi-data type dictionary from a ';' separated file.
    The expected header of the file is:
        variable;value;type
    state_file_name = '/fred/oz996/alex_dev/gwdc_utils/bank_utils/default_bank_generation_setup.csv'

    Current supported types are:
        allowed_state_data_types = ['int', 'string', 'float', 'bool', 'dict']

    The purpose of this function is to update the running state of a python environment. Works a bit like a
    config file but does not have to have each variable extracted+assigned within scope. The same 3 lines work
    (4 lines including the import statement) if you have 5 or 50 variables to load into scope.

    USAGE:
        from bank_utils.base import make_dictionary

        local_dict = make_dictionary()
        for key, value in local_dict.items():
            vars()[key] = value

    @param state_file_name: the setup file containing the variables with default values and types you
                            wish to load into scope

    @return:local_dict: a dictionary built from the entries in state_file_name
    """

    if state_file_name is None:
        state_file_name = '{}/default_bank_generation_setup.csv'.format(
            bank_utils_path)
        print('Loading default state for bank generation from:\n{}'.format(
            state_file_name))

    local_dict = {}
    allowed_state_data_types = ['int', 'string', 'float', 'bool', 'dict']

    try:
        state_file = read_csv(state_file_name, delimiter=';')
    except Exception as local_exception:
        print("Failed to read in: {}".format(state_file_name))
        print("EXCEPTION: {}".format(local_exception))

    for data_type in state_file.type.unique().tolist():
        if data_type not in allowed_state_data_types:
            print(
                "The setup file you passed me contains a variable of type: {}\n"
                .format(data_type))
            print("Current allowed types are: {}".format(
                allowed_state_data_types))
            goodbye(1)

    for local_index in state_file.index.tolist():
        local_data_type = state_file.iloc[local_index].type
        if local_data_type == 'string':
            local_dict[state_file.iloc[local_index].variable] = str(
                state_file.iloc[local_index].value)
        if local_data_type == 'int':
            local_dict[state_file.iloc[local_index].variable] = int(
                state_file.iloc[local_index].value)
        if local_data_type == 'float':
            local_dict[state_file.iloc[local_index].variable] = float(
                state_file.iloc[local_index].value)
        if local_data_type == 'bool':
            local_dict[state_file.iloc[local_index].variable] = bool(
                state_file.iloc[local_index].value)
        if local_data_type == 'dict':
            local_dict[state_file.iloc[local_index].variable] = \
                eval(re.search(r"\{(.*?)\}", state_file.iloc[local_index].value).group(0))

    return local_dict
