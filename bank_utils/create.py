# Copyright (C) 2020-2021 Alex Codoreanu
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import glob
from multiprocessing import Pool
from os.path import isfile

from bank_utils.base import create_err_status_file
from bank_utils.base import create_single_bank_report_file
from bank_utils.base import get_approximant
from bank_utils.base import get_num_jobs
from bank_utils.base import make_dictionary
from glue.ligolw import array
from glue.ligolw import ligolw
from glue.ligolw import param
from glue.ligolw import utils
from gstlal.spiirbank.cbc_template_iir import Bank
from lal.series import read_psd_xmldoc

array.use_in(ligolw.LIGOLWContentHandler)
param.use_in(ligolw.LIGOLWContentHandler)

local_dict = make_dictionary()
for key, value in local_dict.items():
    vars()[key] = value

del local_dict


def err_file_information_report(dir_path, check_logs_string='logs_*'):
    """This is the main function wrapper for creating a series of status report files.
    An example use case can be seen in:
    example_create_dot_err_file_information_report_run.txt

    usage:
        from bank_utils.create import err_file_information_report
        err_file_information_report('/fred/oz016/PSD-1263344418-21600')


    :param dir_path: The directory in which the templates are located
    :param check_logs_string: A string indentifier to find the directories holding the .err files associated with the
    bank generation scripts.
    :return: 0
    """
    check_logs = '{}/{}'.format(dir_path, check_logs_string)

    # find the logs_* directories in the dir_path
    log_directories = glob.glob(check_logs)

    # create a pool job submission for each directory in the log_directories
    # limit it the pool size to 4 because it will run on the farnakle nodes
    try:
        num_tasks = get_num_jobs(len(log_directories), dir_path)
        pool = Pool(num_tasks)

        print '\n\nSuccessfully started a pool with {} threads\n\n'.format(
            num_tasks)
        results = [
            pool.apply_async(create_err_status_file,
                             args=(log_directory, dir_path))
            for log_directory in log_directories
        ]
        pool.close()
        pool.join()
        pool.terminate()
        print type(results)
        del results

    except (IOError, TypeError, ValueError):
        print 'Failed to launch jobs in a distributed pool\n' \
              'and will now run them in sequential order.'
        for log_directory in log_directories:
            create_err_status_file(log_directory, dir_path)

    print "All of your threads have finished\n" \
          "Starting to write report"

    create_single_bank_report_file(dir_path)

    return 0


def select_templates_from_full_banks(reference_psd,
                                     template_bank_name,
                                     output_file_name,
                                     bank_id,
                                     negative_latency=0,
                                     templates=None,
                                     bank_generation_setup_file=None):
    """This function is a more verbose Python version of gstlal_iir_bank. This function is written as to provide a
    single scope in which to test and investigate the performance of the bank generation script.

    The original script was originally written by:
    # Copyright (C) 2011-2012  Shaun Hooper
    # Copyright (C) 2013-2015  Qi Chu

    Additional functionality offered by this wrapper includes:
        - the ability to only create specific templates from the selected Bank using the templates variable
        - the ability to specify if you want to overwrite an existing file with the same name. If not, it will
            continously rename the file until a new version is selected.
            This will ensure that you won't accidentally overwrite files if you forget to check the paths.
        - the ability to keep track of the last template started by using the keep_track_bool boolean. The current
        template number is passed to a file in the same directory as the output.

    The input variables are kept consistent with the original code and can be pulled from the original gstlal_iir_bank
    documentation.

    @param reference_psd:
    @param template_bank_name:
    @param output_file_name:
    @param bank_id:
    @param negative_latency:
    @param templates:
    @param bank_generation_setup_file:

    @return:
    """

    if bank_generation_setup_file is not None:
        state_dict = make_dictionary(
            state_file_name=bank_generation_setup_file)
        for key, value in state_dict.items():
            vars()[key] = value

    if isfile(output_file_name):
        if not overwrite_file:
            file_check_bool = isfile(output_file_name)
            file_check_counter = 1
            while file_check_bool:
                if debug_bool:
                    print 'template file: {} EXISTS'.format(output_file_name)
                output_file_name = output_file_name.replace(
                    '.xml.gz', '_ver_{}.xml.gz'.format(file_check_counter))
                if debug_bool:
                    print 'checking: {}'.format(output_file_name)
                file_check_bool = isfile(output_file_name)
            if debug_bool:
                print 'will write to: {}'.format(output_file_name)

    bank = Bank()
    approximant_string = get_approximant(bank_id)
    ALLpsd = read_psd_xmldoc(
        utils.load_filename(reference_psd,
                            verbose=verbose_bool,
                            contenthandler=ligolw.LIGOLWContentHandler))

    bank.build_from_tmpltbank(template_bank_name,
                              templates=templates,
                              sampleRate=sampleRate,
                              negative_latency=negative_latency,
                              all_psd=ALLpsd,
                              verbose=verbose_bool,
                              padding=padding,
                              flower=flow,
                              snr_cut=snr_cut,
                              downsample=downsample_bool,
                              optimizer_options=optimizer_options,
                              waveform_domain=waveform_domain,
                              approximant=approximant_string,
                              autocorrelation_length=autocorrelation_length,
                              debug=debug_bool,
                              keep_track=keep_track_bool,
                              output_file=output_file_name,
                              **epsilon_options)

    bank.write_to_xml(output_file_name)

    return 0
