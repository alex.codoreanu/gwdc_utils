# Copyright (C) 2020-2021 Alex Codoreanu
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from sys import exit
from os import system as sys


def use_ligolw_to_extract_parameters(local_table_name=None,
                                     local_parameters=None,
                                     file_name=None,
                                     local_output_file_name=None,
                                     print_exec_line=False):
    """This function is a wrapper for the ligolw_print system function that
    appends to a local_output_file_name .csv file with
        COLUMNS:local_parameters extracted from
        TABLE:local_table_name.

    :param local_table_name: the table name from which to extract relevant parameter values
    :param local_parameters: the parameter columns which are to be extracted
    :param file_name: the trigger file name from which to extract from
    :param local_output_file_name: where to output the reshaped table data
    :param print_exec_line: BOOL which prints the ligolw_print line

    :return:
    """

    for local_var in [
            local_table_name, local_parameters, file_name,
            local_output_file_name
    ]:
        if type(local_var) is None:
            print "Incorrect function call\n"
            exit(0)
    #   ligolw_print -t postcoh -c test 000_zerolag_1187008582_334.xml.gz >> test.csv
    execute_line_start = 'ligolw_print -t {} -c '.format(local_table_name)
    parameter_line = ' -c '.join(
        [local_param for local_param in local_parameters])
    execute_line_tail = ' {} >> {}'.format(file_name, local_output_file_name)

    execute_line = '{}{}{}'.format(execute_line_start, parameter_line,
                                   execute_line_tail)  # type: str
    if print_exec_line:
        print execute_line
        print '\n'

    sys(execute_line)


def get_line_elements(local_temp_line, temp_element, split_element=None):
    """This function returns a list object containing all elements in local_temp_line after
    temp_element is deleted. The list is created by splitting local_temp_line by split_element.

    @param local_temp_line: string object to be parsed.
    @param temp_element: element to be deleted from string.
                         ALL INSTANCES WILL BE DELETED.
    @param split_element: string element to split the input line by.
                          Default value is ', '

    @return: local_temp_line.split(split_element)
    """
    if split_element is None:
        split_element = ', '

    local_temp_line = str(local_temp_line).replace(temp_element, '')
    return local_temp_line.split(split_element)


def get_all_variable_flags(all_temp_lines, line_ids=None, verbose=False):
    """This function returns a list of all possible variables present in the whole
    collection in all_temp_lines. These will be the header when extracting the variables
    themselves later on.

    @param all_temp_lines: I/O object containig all lines in question
    @param line_ids: list containing string identifiers to be looked for
    @param verbose: bool to print status

    @return: list of all present labels as identified by the '=' symbol
    """
    # find all possible labels to considers
    if line_ids is None:
        line_ids = ['parser.add_option(']

    labels = []
    for local_temp_line in all_temp_lines:
        for local_line_id in line_ids:
            if local_temp_line.__contains__(local_line_id):
                if verbose:
                    print 'Found : {}'.format(local_line_id)
                    print 'in line:\n{}'.format(local_temp_line)

                elements = get_line_elements(local_temp_line,
                                             local_line_id)[1:]
                for element in elements:
                    if '=' in element:
                        local_variable = element[:element.find('=')].strip(
                        )  # type: str
                        if local_variable not in labels:
                            labels.append(local_variable)

    return labels


def get_output_string(local_temp_line,
                      temp_element,
                      variable_labels,
                      cleanup_elements=None):
    """This function returns a comma separated line with the extracted values for all
    present variable_labels in local_temp_line.

    @param local_temp_line: line to be considered
    @param temp_element: element to be deleted from local_temp_line.
                         To be passed to get_line_elements().
    @param variable_labels: the variables to be searched for.
    @param cleanup_elements: elements to be cleaned up from local_temp_line.
                             Default items are ['\n', '=', ')'].

    @return: a comma separated line containing the values in local_temp_line
    """
    if cleanup_elements is None:
        cleanup_elements = ['\n', '=', ')']

    line_elements = get_line_elements(local_temp_line, temp_element)
    variable_string = line_elements[0].strip()
    variable_string = variable_string.replace('"', '')
    variable_string = variable_string.replace('--', '')

    output_string = '{}'.format(variable_string)

    # now go through variable_labels and find each entry
    for line_element in line_elements[1:]:
        for variable_label in variable_labels:
            output_element = ''
            if line_element[:line_element.find('=')].__contains__(
                    variable_label):
                line_element = line_element.replace(
                    line_element[:line_element.find('=')], '')

                for cleanup_element in cleanup_elements:
                    line_element = line_element.replace(cleanup_element, '')

                line_element = line_element.strip()
                output_element = line_element
                if output_element.__contains__('"'):
                    output_element = output_element.replace('"', '')
                    output_element = '"{}"'.format(output_element)
                    output_element = output_element.replace(',', ';')

            output_string = '{},{}'.format(output_string, output_element)

    return output_string
