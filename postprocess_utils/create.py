# Copyright (C) 2020-2021 Alex Codoreanu
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import glob
import os
from os.path import isfile
from sys import exit

from pandas import read_csv

import base

module_path = os.path.abspath(base.__file__)
module_path = module_path[:module_path.find('/base')]


def combined_dataframe_from_xml_files(xml_path,
                                      xml_id,
                                      setup_file_name=None,
                                      output_file_path=None,
                                      output_file_name=None,
                                      verbose=False,
                                      offline_detector_ids=None):
    """This function identifies all files containing xml_id in directory xml_path.
    It then uses the setup_file_name .csv to identify all TABLE/PARAMETER combinations and
    create a combined __summary.csv file.

    EXAMPLE USAGE:
        from postprocess_utils import create

        xml_path = '/fred/oz016/anwarul/Rerun_devOct17_Feb28/inj/018'
        xml_id = '*zerolag*xml.gz'
        setup_file_name = 'postcoh_setup'

        create.combined_dataframe_from_xml_files(xml_path, xml_id, setup_file_name=setup_file_name, verbose=True)

    @param xml_path: the directory holding the .xml files
    @param xml_id: the substring which identifies the relevant .xml files
    @param setup_file_name: the setup file name which identifies which table and
                            which parameters you wish to extract
    @param output_file_path: the output path for the __summary.csv file
    @param output_file_name: specific file name for the __summary.csv file
    @param offline_detector_ids: list of string identifiers for offline detector related variables
    @param verbose: a BOOL which denotes if you wish to have verbose output during the run of the function

    @return:
    """

    # identify all trigger files
    trigger_files = glob.glob('{}/{}'.format(xml_path, xml_id))

    # read in the job setup file
    if setup_file_name is None:
        print "You must pass a setup file name from which " \
              "to extract the table name from" \
              "which you wish to extract parameter values"
        exit(0)

    setup_file_name = '{}/{}.csv'.format(module_path, setup_file_name)
    setup_file = read_csv(setup_file_name)
    if setup_file.TABLE.unique().size > 1:
        print "Setup file has more than one unique table row.\n"
        exit(2)

    table_name = setup_file.TABLE.unique().tolist()[0]
    parameters = setup_file.PARAMETERS.tolist()

    if offline_detector_ids is not None:
        for offline_detector_id in offline_detector_ids:
            for parameter in parameters:
                if offline_detector_id in parameter[-4:]:
                    parameters.remove(parameter)
                    if verbose:
                        print('Removed {} from parameters list.'.format(
                            parameter))

    if output_file_name is None:
        if output_file_path is None:
            output_file_name = '{}__summary.csv'.format(table_name)
        else:
            output_file_name = '{}/{}__summary.csv'.format(
                output_file_path, table_name)
    if isfile(output_file_name):
        if verbose:
            print "{} already exists and it is being " \
                  "overwritten\n".format(output_file_name)
    else:
        if verbose:
            print "{} writing to file\n".format(output_file_name)

    # write header line
    header_line = '{}\n'.format(','.join([param for param in parameters]))
    with open(output_file_name, 'w') as w:
        w.writelines(header_line)
    if verbose:
        print 'HEADER IS:\n{}\n'.format(header_line)

    # now cycle through all trigger_files
    for trigger_file in trigger_files:
        if verbose:
            print trigger_file
        base.use_ligolw_to_extract_parameters(
            local_table_name=table_name,
            local_parameters=parameters,
            file_name=trigger_file,
            local_output_file_name=output_file_name,
            print_exec_line=verbose)


def backgroundstats_xml_to_csv(infile,
                               setup_file_name=None,
                               output_file_path=None,
                               output_file_name=None,
                               verbose=False,
                               offline_detector_ids=None):
    """This function identifies all files containing xml_id in directory xml_path.
    It then uses the setup_file_name .csv to identify all TABLE/PARAMETER combinations and
    create a combined __summary.csv file.

    EXAMPLE USAGE:
        from postprocess_utils import create

        xml_path = '/fred/oz016/anwarul/Rerun_devOct17_Feb28/inj/018'
        xml_id = 'bank*xml.gz'
        setup_file_name = 'backgroundstats_setup'

        create.backgroundstats_xml_to_csv(xml_path, xml_id, setup_file_name=setup_file_name, verbose=True)

    @param xml_path: the directory holding the .xml files
    @param xml_id: the substring which identifies the relevant .xml files
    @param setup_file_name: the setup file name which identifies which table and
                            which parameters you wish to extract
    @param output_file_path: the output path for the __summary.csv file
    @param output_file_name: specific file name for the __summary.csv file
    @param offline_detector_ids: list of string identifiers for offline detector related variables
    @param verbose: a BOOL which denotes if you wish to have verbose output during the run of the function

    @return:
    """

    # identify all stats files
    stats_files = infile

    # read in the job setup file
    if setup_file_name is None:
        print "You must pass a setup file name from which " \
              "to extract the table name from" \
              "which you wish to extract parameter values"
        exit(0)

    setup_file_name = '{}/{}.csv'.format(module_path, setup_file_name)
    setup_file = read_csv(setup_file_name)
    if setup_file.ARRAY.unique().size > 1:
        print "Setup file has more than one unique array.\n"

    array_name = setup_file.ARRAY.unique().tolist()
    if offline_detector_ids is not None:
        for offline_detector_id in offline_detector_ids:
            for this_array_name in array_name:
                if offline_detector_id in this_array_name:
                    array_name.remove(this_array_name)
                    if verbose:
                        print('Removed {} from array list.'.format(
                            this_array_name))

    # FIX ME: define table_name
    if output_file_name is None:
        if output_file_path is None:
            output_file_name = '{}__summary.csv'.format(table_name)
        else:
            output_file_name = '{}/{}__summary.csv'.format(
                output_file_path, table_name)
    if isfile(output_file_name):
        os.remove(output_file_name)
        if verbose:
            print "{} already exists and it is being " \
                  "overwritten\n".format(output_file_name)
    else:
        if verbose:
            print "{} writing to file\n".format(output_file_name)

    for this_array_name in array_name:
        # write header line
        header_line = '{}\n'.format(this_array_name)
        with open(output_file_name, 'a+') as w:
            w.write(header_line)
        if verbose:
            print 'HEADER IS:\n{}\n'.format(header_line)
        base.ligolw_extract_array(local_array_name=this_array_name,
                                  file_name=infile,
                                  local_output_file_name=output_file_name,
                                  print_exec_line=verbose)


def csv_file_from_parsed_command_line_arguments(reference_file_name,
                                                output_file_name,
                                                default_line_ids=None,
                                                verbose=True):
    """This function creates a .csv data structure comprised of all identified parser.add_option
    flags in reference_file_name.

    USAGE:

        reference_file_name = 'original_parse_arguments.py'
        output_file_name = 'parsed_arguments.csv'

        from postprocess_utils.create import csv_file_from_parsed_command_line_arguments

        csv_file_from_parsed_command_line_arguments(reference_file_name, output_file_name)


    @param reference_file_name: file to be investigated
    @param output_file_name: output csv file
    @param default_line_ids: list containing elements
                             which can identify a line of interest
    @param verbose: bool to track progress

    @return:
    """
    if default_line_ids is None:
        default_line_ids = ['parser.add_option(']

    reference_file = open(reference_file_name, 'r')
    all_lines = reference_file.readlines()
    print 'Read in reference_file:{}'.format(reference_file.name)

    variable_labels = base.get_all_variable_flags(all_lines)

    # write header
    header_line = 'variable,{}\n'.format(','.join(variable_labels))
    with open(output_file_name, 'w') as w:
        w.writelines(header_line)

    # go through each line and identify all found variables
    for local_line in all_lines:
        for el in default_line_ids:
            if local_line.__contains__(el):
                if verbose:
                    print 'Found : {}'.format(el)
                    print 'in line:\n{}'.format(local_line)

                local_output_string = base.get_output_string(
                    local_line, el, variable_labels)

                if verbose:
                    print local_output_string

                with open(output_file_name, 'a') as a:
                    a.writelines('{}\n'.format(local_output_string))

                print '\n\nFinished writing to:\n{}\n'.format(output_file_name)
