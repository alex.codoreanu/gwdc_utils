\contentsline {section}{\numberline {1}Introduction}{3}{section.1}% 
\contentsline {section}{\numberline {2}Ozstar Supercomputer}{4}{section.2}% 
\contentsline {subsection}{\numberline {2.1}OzStar environment}{5}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Development/testing on OzStar}{6}{subsection.2.2}% 
\contentsline {section}{\numberline {3}GWDC Utilities}{8}{section.3}% 
\contentsline {section}{\numberline {4}Design reference material}{9}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Detection workflow}{9}{subsection.4.1}% 
\contentsline {section}{\numberline {A}Bash tips}{12}{appendix.A}% 
\contentsline {subsection}{\numberline {A.1}for your local machine}{12}{subsection.A.1}% 
\contentsline {subsection}{\numberline {A.2}for your remote OzStar environment}{12}{subsection.A.2}% 
\contentsline {section}{\numberline {B}Build the SPIIR pipeline notes}{14}{appendix.B}% 
\contentsline {subsection}{\numberline {B.1}Build script notes}{14}{subsection.B.1}% 
\contentsline {section}{\numberline {C}Setup PyCharm on OzStar and use the SPIIR pipeline}{15}{appendix.C}% 
\contentsline {subsection}{\numberline {C.1}Introduction}{15}{subsection.C.1}% 
\contentsline {subsection}{\numberline {C.2}Intial Setup Requirements}{16}{subsection.C.2}% 
\contentsline {subsection}{\numberline {C.3}SPIIR environment}{16}{subsection.C.3}% 
\contentsline {subsection}{\numberline {C.4}Connect local PyCharm distribution to OzStar cluster}{17}{subsection.C.4}% 
\contentsline {subsection}{\numberline {C.5}Synchronise local files to remote cluster}{18}{subsection.C.5}% 
\contentsline {subsection}{\numberline {C.6}Run/Debug configuration}{18}{subsection.C.6}% 
\contentsline {subsection}{\numberline {C.7}Some productivity suggestions}{19}{subsection.C.7}% 
\contentsline {section}{\numberline {D}Connect PyCharm to job allocation resources}{20}{appendix.D}% 
\contentsline {subsection}{\numberline {D.1}Get a remote job allocation}{20}{subsection.D.1}% 
\contentsline {subsection}{\numberline {D.2}Connect an assigned resource to the login node}{20}{subsection.D.2}% 
\contentsline {subsection}{\numberline {D.3}Connect local interpreter to assigned resource}{21}{subsection.D.3}% 
\contentsline {section}{\numberline {E}Code style}{22}{appendix.E}% 
\contentsline {section}{\numberline {F}Git workflow}{23}{appendix.F}% 
\contentsline {subsection}{\numberline {F.1}Using \texttt {git}{} and GitLab}{24}{subsection.F.1}% 
