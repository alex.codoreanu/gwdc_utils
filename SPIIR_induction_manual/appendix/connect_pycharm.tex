
\section{Setup PyCharm on OzStar and use the SPIIR pipeline}
\subsection{Introduction}
\indent Working with remote compute resources introduces an additional layer of complexity that must be integrated into a  fluid workflow. There are several options which can help with this, each with their own benefits and drawbacks:

\begin{enumerate}
	\item use an editor hosted on the remote machine (VIM/EMACS):
	\begin{enumerate}
		\item {\bf Benefits}: very light weight and it's always there, at least on the OzStar/CIT clusters and on most Unix distributions. Can be easily customised, there are many helpful YouTube\footnote{\href{https://www.youtube.com/watch?v=IiwGbcd8S7I}{a one hour VIM tutorial}}\footnote{\href{https://www.youtube.com/watch?v=5givLEMcINQ&list=PL13bz4SHGmRxlZVmWQ9DvXo1fEg4UdGkr}{a more comprehensive VIM tutorial list}} tutorials and provides a keyboard only workflow that can provide a very fast way to interact with a codebase.
		
		\item {\bf Drawbacks}: there are not many drawbacks resulting from the actual use itself but current IDE\footnote{Integrated Development Environment} workflows provide additional functionality such as syncing local+remote code, version control and additional debug and code exploration tools.
	\end{enumerate}

	\item use a contemporary IDE: \href{https://www.jetbrains.com/pycharm/}{PyCharm}:
	\begin{enumerate}
		\item {\bf Benefits}: A contemporary IDE provides several key functional features that make it a great option for working with remote codebases. Some of these features are;
		\begin{enumerate}
			\item Continous code synchronisations
			\item Version Control Management
			\item Remote interpreter/s
			\item Code navigation
			\item Integrated Debug and Profile features
		\end{enumerate}
		
		\item {\bf Drawbacks}: Unlike a built in lightweight editor, this has to be downloaded and properly setup before getting the above benefits. Some of the features are only available in the Professional version of PyCharm and may not be available in some other IDEs such as VsCode. Luckily, any .edu email holder qualifies for a free Professional version of PyCharm. Additionally, I'd recommend a minimum of 8gb of RAM for the machine hosting the distribution.
		
	\end{enumerate}		

	\item use a local text editor paired with a RSYNC or SCP custom pipeline:
	\begin{enumerate}
		\item {\bf Benefits}: you can continue to use your preferred editor such as Sublime or Textmate. These tend to be more lightweight and can easily run on machines with 4gb or RAM or even less. They often provide some code navigation aids and can integrate with version control subsystems.
		
		\item {\bf Drawbacks}: they do not allow the use of remote interpreters and do not have advanced debug and/or profiler options. Additionally, the code syncing can be an issue if not properly monitored. In general, the additional general supervision required is generally offset by the initial time investment in just setting up an IDE rather than choosing this option.
		
	\end{enumerate}

\end{enumerate}

Whichever option describes your workflow, there is no absolute right or wrong way but this document will focus on how to set-up a professional version of PyCharm to work with OzStar cluster resources \footnote{although any remote resource could be substituted} and interact with the SPIIR codebase. I'll discuss the initial download and setup of a professional PyCharm distribution in Section \ref{sec:setup_requirements}. I'll describe how to use the SPIIR specific environment in Section \ref{sec:activate}. In Section \ref{sec:connect}, I'll describe how to use the login as well as custom job allocation resources as a development/working environment. \\

This is a working document and please reach out\footnote{\href{mailto:acodoreanu@swin.edu.au}{Alex Codoreanu, acodoreanu@swin.edu.au}} with any comments or requests for additions.

\subsection{Intial Setup Requirements}
\label{sec:setup_requirements}

PyCharm is one of many JetBrains products and can be downloaded from \href{https://www.jetbrains.com/pycharm/download}{here}. A local Python distribution should already be installed in order for a local interpreter to be available. If you don't have a local Python distribution, I'd recommend \href{https://www.anaconda.com/distribution/}{using the Anaconda installation}. For a more lightweight distribution you can use the \href{https://docs.conda.io/en/latest/miniconda.html}{Miniconda installer}. The SPIIR pipeline uses Python2.7. Now that local Python and Pycharm distributions have been downloaded/installed we can begin the setup process. \\

The setup process has the following steps:
\begin{enumerate}
	\item set-up the OzStar SPIIR working environment so that PyCharm can use it as a remote resource (section \ref{sec:activate})
	\item connect the remote interpreter to the OzStar login nodes (section \ref{sec:connect})
	\item setup synchronisation of files (section \ref{sec:sync})
	\item edit RUN/DEBUG configurations (section \ref{sec:run_debug_config})
	\item some general option suggestions (section \ref{sec:productivity})
  
	
\end{enumerate}


\subsection{SPIIR environment}
\label{sec:activate}

It is assumed that you have a working account on the OzStar cluster. If you do not then you must create one (see section \ref{sec:intro}). \\

Once the SPIIR pipeline has been downloaded and installed (section \ref{sec:build}) you can set it as the default working environment on OzStar\footnote{this means that as soon as you log in, you are able to run SPIIR specific function calls}. It is important to note that in order for the continuous synchronization of files and debug modes to work, THERE MUST NOT BE ANY VERBOSE MESSAGING WHEN LOGGING IN TO OzStar\footnote{\it{rsync} will break down if there is no device to output messages to}. The recommended approach is to simply add the respective {\it{load\_spiir\_\{{\it \bf{branch}}\} }}function call at the end of your {\it .bashrc} file. If you have multiple SPIIR branches available, then simply comment out or delete the function calls to the branches you're not using. In order to validate that your current distribution has succesfully been deployed run: \href{https://git.ligo.org/alex.codoreanu/gwdc_utils/-/blob/master/testImports.py}{testImports.py}). \\


If you are not using the build process described in section \ref{sec:build} then:

\begin{enumerate}
	\item take the necessary steps to get to an interactive working environment, such as sourcing a single or multiple {\it .bashrc} files. Test that your environment is working by simply running \href{https://git.ligo.org/alex.codoreanu/gwdc_utils/-/blob/master/testImports.py}{testImports.py}.
	
	\item identify the value of the following environment variables\footnote{to view the current value of an environment variable simply run {\it echo \$VARIABLE}}:
	\begin{enumerate}
		\item PYTHONPATH
		\item PATH
		\item LD\_LIBRARY\_PATH
		\item GST\_PLUGIN\_PATH
		\item LAL\_DATA\_PATH
		\item LIBRARY\_PATH
		\item PKG\_CONFIG\_PATH
		\item LAL\_DATA\_PATH
		
		
	\end{enumerate}
	
	\item export\footnote{use {\it cat} or just copy\textbackslash paste} them into your default {\it .bashrc} file\footnote{you can wrap them in a function and then call the function or simply put them in as environment variables at the end fo the file}
	
	\item exit OzStar
	
	\item log back in and run {\it python testImports.py}. If you can successfully run this then you are ready to use OzStar as a remote interpreter for your local PyCharm distribution.
	
\end{enumerate}


\subsection{Connect local PyCharm distribution to OzStar cluster}
\label{sec:connect}
There will be slight differences whether you are starting your first or already have an existing PyCharm project that you wish to connect to a remote interpreter. If this your first PyCharm project, I would recommend using the local interpreter when starting the project and then setting up the remote interpreter following the steps below.\\
 
To connect a remote interpreter to your local Pycharm distribution:
\begin{enumerate}
	\item open {\bf PyCharm\textbackslash Preferences}
	\item expand {\bf Project: ---} section
	\item select {\bf Project Interpreter}
	\item click the wheel icon of the Project Interpreter tab
	\item select {\bf Add ...}
	\item click {\bf SSH Interpreter} from the new {\it Add Python Interpreter} window spawned by the previous action 
	\item Start a New server configuration with: 
	
	\indent \indent \indent - {\bf Host}: ozstar.swin.edu.au
	
	\indent \indent \indent - {\bf Port}: 22
	
	\indent \indent \indent - {\bf Username}: {\it your username}
	
	\item click {\bf Next}\footnote{if you are having any issues here then you might need to update or downgrade your {\it OpenSSL} and\textbackslash or {\it OpenSSH} distribution.}
	
	\item If you have not set up a private key for OzStar\footnote{\href{http://www.linuxproblem.org/art_9.html}{SSH login without password}} then enter you Password and ensure that the {\it Save password} box is clicked.
	
	\item click {\bf OK}
	
	\item select your python executable. If you are using the system python2.7 version then the executable is: 
	
	{\it /apps/skylake/software/compiler/gcc/6.4.0/python/2.7.14/bin/python}

	MAKE SURE YOU NAVIGATE AND CLICK ON THE EXECUTABLE RATHER THAN JUST COPYING AND PASTING THE ABOVE.
	
	\item click {\bf OK}
	
	\item you should now be back in the original {\it Preferences} window where a new {\it Path Mappings} section is visible. Click on the folder icon and navigate to the remote location where you would like your documents to be placed. I would not recommend using your home folder. You should have access to {\it oz016} or {\it oz996} if you've followed all of the steps in section \ref{sec:intro}.

\end{enumerate}


\subsection{Synchronise local files to remote cluster}
\label{sec:sync}

A very useful feature provided by modern IDE is {\it continuous} or {\it on save action} code synchronisation. This means that any local code changes will be reflected in your remote codebase. In order to achieve this in Pycharm you can go through the following steps:

\begin{enumerate}
	\item in the top bar, click {\bf Tools\textbackslash Deployment\textbackslash Configuration...}
	
	\item click on the {\bf +} symbol below the close window button to start a new configuration
	
	\item I would not recommend clicking on the {\it Visible only for this project} box as you can reuse the deployment configuration for other projects
	
	\item select {\it Type: SFTP}
	
	\item {\it HOST: ozstar.swin.edu.au  PORT:22} 
	
	\item {\it User name:} your ozstar user name
	
	\item {\it Authentification:} same as with the interpreter, you can put in your password or point to your OpenSSH key
	
	\item click {\it Test Connection}. If successful then proceed to the next steps \\
	
	\item click on the {\it Mappings} tab
	
	\item click on the folder icon at the right of the {\it Deployment path}
	
	\item navigate to the remote folder that you want your local codebase to be uploaded to.
	\begin{tcolorbox}[colback=yellow!10!white ,
colframe=white!20!black]
	\begin{center}
			\textsc{Please Note:}

	
	\textsc{Whatever is at the end of your remote path will be deleted and replaced by everything in the last sub-directory of your local path.}
	
	\end{center}	
	\end{tcolorbox}

\end{enumerate}



\subsection{Run/Debug configuration}
\label{sec:run_debug_config}

Most environment variables will be automatically picked up by the remote python interpreter but, unfortunately {\bf PYTHONPATH} is not one of them. The remote interpreter only looks in the site-packages repository associated with the python executable. Thus, in order to run and debug remote code from your local IDE, you must give it the appropriate path.

\begin{tcolorbox}[colback=yellow!10!white ,
colframe=white!20!black]
	
	\begin{center}
		\textsc{Please Note:}
	\end{center}
	
	Given that the each branch of the pipeline will most likely have a different installation location (if you've used the GWDC installation) and that different base python modules are loaded it is highly recommended that you explicitly attach the remote PYTHONPATH variable paths to the chosen interpreter. This will allow you to effectively switch branches and test any new code that you're writing or to debug any member of the chosen branch. \\
	
	{\bf PYTHONPATH is not picked up automatically so if switch branches using the {\it load\_spiir} bash function, you must also change the PYTHON path variable in Run/Edit Configurations.\\}
	
	In order to accomplish the above, you must first identify your new full PYTHONPATH\footnote{{\it echo PYTHONPATH} in your new working environment} and then:
	
	\begin{enumerate}
		
		\item open {\bf PyCharm\textbackslash Run\textbackslash Edit Configuration}
		
		\item click the list icon to the right of the {\it Environment Variables} box
		
		\item click the {\bf +} icon in the {\it Environment Variables} dialogue box
		
		\item enter PYTHONPATH in the new variable name
		
		\item paste the full PYTHONPATH in the variable field
		
	\end{enumerate}
	
	I recommend that you create a new folder for each new branch and move the related configurations into these folders. Thus, you only need to set this up once. 
	
\end{tcolorbox}

\subsection{Some productivity suggestions}
\label{sec:productivity}

One keyboard map suggestion that is quite useful is to clone caret above/below. This, in combination with select to the end of line, will give you the option to select the full scope of a function/for loop of any given code selection.


