
ozstar_user='<your ozstar user name>'
cit_user="<your cit user name>"

# ssh into home directory
alias oz="ssh -X -t -Y -C ${ozstar_user}@ozstar.swin.edu.au"
#sftp into home directory
alias sftpoz='sftp ${ozstar_user}@ozstar.swin.edu.au'

# jupyter notebook shortcut
alias jp='jupyter notebook'

# ssh into CIT cluster
alias cit='ssh -X -t -Y -C ${cit_user}@ldas-grid.ligo.caltech.edu'

# add some common option flags for general functions
alias ls='ls -Gfh --color'
alias la='ls -lah --color'
alias rm="rm -i"
alias cp="cp -i"
alias mv="mv -i"
alias scp="scp -p"
alias ssh="ssh -Y -C"

