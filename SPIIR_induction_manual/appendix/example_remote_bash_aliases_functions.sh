
# User specific aliases and functions
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

alias ozstar_user="<your Ozstar user name>"
alias uwa_spiir="cd /fred/oz016/"
alias gwdc_spiir="cd /fred/oz996/"

# set up alias for my queue
alias mq='squeue -u ${ozstar_user}'

# create alias for top | grep me
alias mt='top | grep ${ozstar_user}'

# cancel all of my jobs
alias cancel_all='scancel -u ${ozstar_user}'

# remove all .out files in directory
alias clean_out='rm -rf *.out; echo "removed all .out files"'

# create alias for top | grep me
alias mt='top | grep ${ozstar_user}'

alias workworkworkworkwork="sinteractive --mem=5gb --time=08:00:00"

alias gv="ghostscript"

alias ls='ls -Gfh --color'
alias la='ls -lah --color'

alias rm="rm -i"
alias cp="cp -i"
alias mv="mv -i"
alias scp="scp -p"
alias ssh="ssh -Y -C"
