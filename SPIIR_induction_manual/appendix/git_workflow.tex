\newcommand{\master}{\texttt{master}}
\newcommand{\git}{\texttt{git}}
\section{Git workflow}
\label{sec:gitworkflow}

This appendix documents the preferred workflow for using \git{} and
GitLab to manage development on the SPIIR pipeline.

This workflow is designed to manage some of the key requirements for SPIIR
development:
\begin{itemize}
	\item Stability: SPIIR goes through a significant LIGO Scientific
		Collaboration review process, and is a key pipeline for low-latency
		detection of gravitational waves. Accordingly, the reviewed code must
		be kept stable and reliable
	\item Fast cadence: New development should make it into SPIIR rapidly
	\item Correctness: Generally, code that goes into SPIIR should be inspected
		by people other than its authors
\end{itemize}
Throughout this section, we attempt to consistently use `review' to mean an
external review by the collaboration, and `inspection' to mean an internal
review by other members of the SPIIR team.

It is worth noting that the process described in this document is not the
\textit{only} correct way to use \git{}; however it is the method adopted for
SPIIR.

Figure~\ref{fig:branchflow} is a conceptual overview of how the development of
SPIIR should work. (Below, we will explain how to actual use \git{} to achieve
this concept.)
Fundamentally, there are two main long-live branches:
\begin{itemize}
	\item \master{} represents the most highly developed, but complete, version
		of SPIIR; and
	\item \texttt{reviewed} represents the latest version of SPIIR that has
		been reviewed by the Collaboration for production use.
\end{itemize}
Periodically, the code in \master{} is reviewed
(externally, by the LIGO Scientific
Collaboration). When this occurs, if the code is approved, the \master{}
branch will be merged into the \texttt{reviewed} branch. In rare cases, only
part of the code will be merged, where the review approves some parts but not
others. Tags of the \texttt{reviewed} branch are used to capture what version
of the code was used for production searches during each observing run.

New features, bug fixes, and other changes to the code should be developed by
individuals in a separate branch per feature. When the feature is complete
enough that it deserves to be considered as part of SPIIR, that feature branch
will be merged back into \master{}. This merge process involves an internal
inspection by another SPIIR developer (preferably an experienced one) and is
managed through the LIGO GitLab.

Some rules of thumb:
\begin{itemize}
	\item Each feature should be a small piece of work, in general, of the size
		that can be developed in a week or less
	\item Conversely, each feature should be complete in the sense that when
		merged into \master{}, it will not imperil the stability or correctness
		of SPIIR
	\item Each branch should be used for only one feature
	\item Feature branches should live not much longer than a week
	\item Where possible, rebase before merging
	\item In fact, you should rebase regularly, to keep up with the codebase
		and to keep on top of what other people are doing
\end{itemize}
In some cases, where large features are developed, it may be appropriate to
create a long-lived branch for that feature, and develop individual features as
branches off the long-lived one. This should happen only when the extra difficulty in
eventually merging the long-lived branch into \master{}, which must necessarily
occur some months later, can be justified.

\subsection{Using \git{} and GitLab}
First, to get set up, you will need to clone the repository\footnote{Here we
show how to use the \texttt{build-spiir.sh} script to clone the repository. If
you have special requirements, you want to directly clone the repository:\\
\texttt{git clone git@git.ligo.org:lscsoft/spiir.git}
} and create the branch to do development in:
\begin{lstlisting}
$ ./build-spiir.sh build-spiir --branch master --name mybranch
$ cd_spiir mybranch
$ git checkout -b pwc_newtriggercontrol
\end{lstlisting}
The ``\texttt{checkout -b} command is the command that creates the new branch
for the feature you're working on, and switches you working copy to it.  The
branch will be short-lived so you don't need to put a lot of effort into naming
it, but it's good practice to prefix it with your name or initials to avoid it
being confused with everyone else's branches.

\begin{figure}
	\includegraphics[width=\textwidth]{../images/git_diagrams/concept.pdf}
	\caption{Conceptual illustration of the branching workflow. New features
	are created in short-lived branches from \master{}. These branches live
	only a short time and are merged back to \master{} regularly. Periodically,
	LSC code reviews result in \master{} being merged into \texttt{reviewed}.}
	\label{fig:branchflow}
\end{figure}

You can then implement the feature, and get it to a state where it is
ready to be merged back into \master{}. You can always check which branch
you're on by running \texttt{git branch}.

In doing this, you might create many individual commits (with \texttt{git
commit}); how you manage your own development is entirely up to you. We will,
however, ``squash'' the commits down into a single commit in preparation for
merging.

Similarly, while you are developing code on your branch, others will be
developing code on their own branches. To make it easier on the person
inspecting your changes, you should ``rebase'' your changes on to the latest
\master{}, prior to submitting a merge request. Suppose you are Alice, and Bob
is another developer; both of you begin working on SPIIR features at the same
time, but Bob finishes first and his changes are merged in earlier:
\begin{center}
	\includegraphics[width=0.8\textwidth]{../images/git_diagrams/before_rebase.pdf}
\end{center}
If you were to submit a merge request now, the approver would be responsible
for ensuring that your changes don't conflict with Bob's. To avoid this, you
can ``rebase'' your changes:
\begin{center}
	\includegraphics[width=0.8\textwidth]{../images/git_diagrams/after_rebase.pdf}
\end{center}
Once this is done, you will be able to submit a clean merge request. (Note that
sometimes, merging is unavoidable: for example, if Bob had submitted his merge
request, but it hadn't been approved by the time you submitted yours.)

To rebase your branch (assuming you are on your feature branch, and have
committed all your changes).
\begin{lstlisting}
$ git checkout master # switch to master branch
$ git pull origin master # pull latest changes
$ git checkout pwc_newtriggercontrol # switch back to feature branch
$ git rebase -i master # rebase feature branch on master
\end{lstlisting}
The `\texttt{-i}'' switch to \texttt{rebase} tells \git{} that we want to
change the commit messages, in preparation for submitting a merge request. This
is because, as discussed above, your merge request should consist of a single
commit. \git{} will open your editor with a series of lines like:
\begin{lstlisting}
pick 16651f9 Started owrk on new trigger control approach
pick fe7cd57 did some work
pick 3a32fe4 final version for commit
\end{lstlisting}
representing your existing commits. We want to change the commit message for
the first commit, and meld all the other commits together, so replace the first
\texttt{pick} with \texttt{reword} and the rest with \texttt{fixup}:
\begin{lstlisting}
reword 16651f9 Started owrk on new trigger control approach
fixup fe7cd57 did some work
fixup 3a32fe4 final version for commit
\end{lstlisting}
When you save and quit your editor, \git{} will open a new editor that will let
you adjust the commit message.

The commit message for the merge should reference the relevant JIRA ticket,
contain a short ($\sim50$ character) subject, and a brief but comprehensive
description of your changes. For example:
\begin{lstlisting}
[SPIIP-72] Trigger control: skymap in separate cluster job

When a new event is found, the skymap job is now run as a separate
supercomputer job rather than on the login node. This is done right after the
event has been submitted to GraceDB (so the new job has access to the GraceDB
ID). A new command line parameter, --medium-latency-followup, can be set to
'slurm', 'condor', 'ignore' or 'loginnode' (old approach) to control what kind
of job is created.

The change is further described in the document "Design of event
clustering/trigger control for SPIIR pipline" in section 3.
\end{lstlisting}
You are now ready to create a merge request. Upload your branch to GitLab with:
\begin{lstlisting}
$ git push origin pwc_newtriggercontrol
\end{lstlisting}
Then, visit the GitLab website (\texttt{git.ligo.org}), click through to
`Branches', and click `Merge request':
\begin{center}
	\includegraphics[width=0.7\textwidth]{../images/git_diagrams/branches.png}
\end{center}
You can then select who\footnote{TBD}
should approve your request, and press `Submit merge request'.
\begin{center}
	\includegraphics[width=0.7\textwidth]{../images/git_diagrams/merge_request.png}
\end{center}
It's then up to that person to inspect your merge request. If they're satisfied
with it, they will approve it and merge it; otherwise, they will write to you
(using GitLab's commenting features) to request clarification or further
changes.
