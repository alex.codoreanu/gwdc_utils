\section{Design reference material}
This section of the induction guide gather collected notes on the design of the
SPIIR pipeline. As the pipeline is a continually-evolving piece of work, this
section evolves with it.

\subsection{Detection workflow}
Figure~\ref{fig:designref:detwf} shows a schematic view of how detections
are made, and the process SPIIR follows when an event is detected.

Detector data originate from sites (top of diagram), and are packaged up and
ultimately fed into the many nodes that are needed to run the full SPIIR
pipeline. (For a non--negative-latency search, typically on CIT there are about
17 nodes for each astrophysical target type.) Each node is running many
(typically on the order of $10^2$) different filter banks, each of which has a
slight different response to an actual astrophysical signal. The data are
processed against each bank in realtime.

When an event occurs, it will typically be picked up in multiple banks. (This is
because there is considerable overlap, in sensitivity to the astrophysical
parameter space, between the banks.) These candidate detections are depicted by
the red dots in the diagram. As part of the detection process, for each
triggered bank there is a crude skymap based on the coherent signal-to-noise
ratio (SNR), which is necessary for creating the more-refined skymap below.

A clustering algorithm is used to determine which of the candidates is the
``best'' fit (i.e., highest SNR). It is worth noting that a significant
constraint here is that the events may not come in at precisely the same time
(either because the measurement uncertainty for ``event end time'' will
naturally produce slightly different answers when different filters are used,
or because event end time is degenerate with other parameters that are
mis-estimated by some banks). The result of the clustering algorithm is the
single best event for all banks on a single node (depicted as a red dot with an
asterisk in the diagram).
[TBD: Describe clustering algorithm.]

If the event is strong, or if the event parameters happen to fall near a
boundary in how the template banks are divided between nodes, then an event may
appear in banks that are being processed by multiple nodes. For this reason,
events are written to a ``trigger control file'' on the shared filesystem,
which will suppress multiple adjacent triggers. In the diagram, this is
depicted as the red dot with a dagger, which is suppressed during trigger
control.
[TBD: Describe trigger control algorithm.]

After trigger control is completed, the pipeline is left with a single
candidate, which needs to be reported as a detection. This workflow is depicted
in the lower right of the diagram.

All LSC pipelines report events/superevents/candidate events through a single
database called GraceDB (Gravitational wave Candidate Event Database) which is
centrally managed by the collaboration. SPIIR reports events to GraceDB in four
stages. Three of these are handled directly by the pipeline code:
\begin{itemize}
	\item Initial upload, with basic parameters such as false alarm rate, bank
		ID, which interferometers were involved, time, etc. Upon first upload,
		the SPIIR pipeline received a unique GraceDB ID for the event.
	\item Upload of optimal right ascension and declination (initial, coarse
		location estimate)
	\item Upload of power spectral density of the detector for each
		interferometer
	\item (A fourth, generating and uploading a skymap, exists in the code but
		is disabled.)
\end{itemize}
The fourth, skymap generation, is handled separately because it is a
computationally- and memory-heavy process. The current design for this uses
\texttt{lvalert} (a separately-maintained XMPP `pubsub' based process)
to monitor GraceDB for the arrival of candidate events, and
generates the skymaps on the CIT headnode for upload to GraceDB.

Future/ongoing work:
\begin{itemize}
	\item Resolve potential race conditions in trigger control
	\item Document cluster algorithm
	\item Improve latency-to-skymap by beginning skymap generation earlier in
		the pipeline
	\item Develop a single daemon that will unify the two processes (clustering
		and trigger control) which are responsible for identifying a single
		event from many candidates, without sacrificing latency
\end{itemize}

\begin{figure}
	\includegraphics[width=0.8\textwidth]{images/design_diagrams/detection_workflow.pdf}
	\caption{Workflow for detections. See in-text description.}
	\label{fig:designref:detwf}
\end{figure}

\subsection{Python 2 to 3 transition plan}
Currently, SPIIR works only with Python~2. As Python~2 has now reached its end
of life, this section documents the approach to migrating to Python~3.

Where possible, all new code will be written so that it is compatible with both
Python~2 and~3. Where this is impossible, the code should be clearly marked
with relevant notes about what needs to change during the transition.

Ancillary pieces of SPIIR (i.e., pieces that are not part of the pipeline
proper, but are invoked separately and are not subject to LSC review) are to be
progressively rewritten in Python~3. Because they are invoked separately from
the pipeline, they can be invoked from a separate Python~3 environment without
disturbing the pipeline itself. In some cases, external scripts will depend on
parts of the pipeline that are Python-2--only; these will continue to be
written in Python~2.

SPIIR itself will remain Python~2 only in the short term. New feature
development will need to be Python~2 compatible, and we will continue to
maintain a Python~2 environment to run the pipeline (including in its
production form). This is the pipeline that will be taken to the review for O4,
expected to begin around January 2021.

Once that review process has started, a separate branch will be split off to
maintain the O4-reviewed, Python~2 code. The main development branch will then
be upgraded to Python~3 and Gstreamer~1.0 (and post-O4 features will be
developed on this branch). This branch will become the production SPIIR after
it has been reviewed---perhaps halfway through O4, otherwise, for the beginning
of O5.
