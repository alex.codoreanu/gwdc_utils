\section{Ozstar Supercomputer}

OzStar is a national computing facility hosted and managed by Swinburne University. It consists of 107 standard compute nodes (36 threads, 2 NVIDIA P100 12gb PCIe GPUs, 192 GB DDR4 RAM) and 8 {\it Data Crunching} nodes in total with the same GPUs and CPUs but with the RAM memory increased to 384Gb and 768Gb for 4 nodes each. \\

Of the 107 login nodes two are used as standard login nodes and these are named farnakle 1/2. These two nodes are the default login connection when you connect from your local machine\footnote{\href{http://www.linuxproblem.org/art_9.html}{how to ssh without password}\footnote{http://www.linuxproblem.org/art\_9.html}}: \\

{\bf ssh -X -Y -t -C <your\_username>@ozstar.swin.edu.au}
\\

where \\

{\bf- X}: enables X11 forwarding which helps with displaying images through the ssh connection. MUST BE CAPITALISED as lowercase {\bf- x} option actually disables X11 forwarding\\

{\bf- Y}: enables trusted X11 forwarding \\

{\bf- t}: forces pseudo-tty allocation. This is very useful for using any additional screen based programs such as launching a script through a {\it screen} session.\\



{\bf- C}: compresses data packets \\
\begin{tcolorbox}[colback=yellow!10!white ,
colframe=white!20!black]

The default login hard disk space will be in your HOME directory\footnote{{\it /home/<your\_username>}}. This directory is one of the two types of locations available to you and is referred to as {\it \bf user folder} in Fig. \ref{fig:ozstarfig}. It is the default location for your {\it .bashrc} hidden bash configuration file. \\

THIS IS THE ONLY BACKED UP DIRECTORY. \\

\end{tcolorbox}

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{ozstar_image.png}
    \caption{Visual representation of the OzStar cluster. Purple denotes a computing resource and yellow highlights the disk space available. The login nodes are {\it farnakle1/2}.}
    \label{fig:ozstarfig}
\end{figure}

The secondary hard disk option will the {\it project folder} location. Each project directory is allocated 10Tb of hard disk storage that is available to all approved project members. Once your user account has been processed then you can login to your account (through the same portal) and {\it Request To Join A Project}. Please allow about 5 working days for both steps to be finalised. The timeline depends on email prompts and requests going back and forth between the OzStar team and the project supervisors. \\

\begin{tcolorbox}[colback=yellow!10!white ,
colframe=white!20!black]
There are 2 SPIIR related projects to join on OzStar:

- oz016 -- the UWA initiated project where most production code is located

- oz996 -- Swinburne GWDC initiated project to test and develop SPIIR related code \\

THESE DIRECTORIES ARE NOT BACKED UP.

\end{tcolorbox}

Your home and project folders are physical storage areas to which you can write to and are visible to both login and other compute nodes. Compute resource/nodes have to be requested and you can \href{https://supercomputing.swin.edu.au/docs/2-ozstar/oz-slurm-basics.html}{find a brief introduction to using SLURM here.}\footnote{https://supercomputing.swin.edu.au/docs/2-ozstar/oz-slurm-basics.html} \\

So, the general picture is that OzStar has 2 login nodes from which you can request specific interactive/non-interactive compute resources. These resources, along with the login nodes, have access to the LUSTRE file system that points to your local home directory or any project directory you have access to. The default working environment is defined through the {\it .bashrc} file in your home directory. I'll discuss how to set this up in the following section (sec. \ref{sec:ozstar_env}).



\subsection{OzStar environment}
\label{sec:ozstar_env}

The Swinburne GWDC team has streamlined and automated the SPIIR build process so that one single version of each git branch is physically installed and available to use for development and testing on OzStar. \\

\begin{tcolorbox}[colback=yellow!10!white ,
colframe=white!20!black]
\begin{center}
    ------------------------------------------------------
    
    DO NOT\\BUILD THE PIPELINE OR ANY BRANCHES\\IF YOU DO NOT NEED TO
    
    ------------------------------------------------------
\end{center}
    


In order to use the SPIIR pipeline build on OzStar you can simply add the following at the tail of your {\it .bashrc} file: \\

. /fred/oz016/gwdc\_spiir\_pipeline\_codebase/scripts\_n\_things/build/bash\_helper\_functions.sh \\

load\_spiir <SPIIR branch name>\footnote{if you leave this blank, it will load the {\it master} branch} \\

\end{tcolorbox}

The above {\it load\_spiir} bash function allows a user to effectively change their SPIIR working environment by adjusting the relevant environment variables to point to the physically stored SPIIR branches. If you change your environment through the command line then, the next time you log in, you will be in the SPIIR environment defined in the {\it load\_spiir <SPIIR branch name>} function call or your {\it .bashrc} file.\\

The currently available SPIIR branches are visible in : \\

/fred/oz016/gwdc\_spiir\_pipeline\_codebase/scripts\_n\_things/build/ \\

\noindent and can also be accessed through the {\it ls\_spiir} bash function. You can also change directories to any of the branches by using the {\it cd\_spiir} bash function. It has the same usage as {\it load\_spiir}. You can also use {\it help\_spiir} to get this information at any point.

\subsection{Development/testing on OzStar}

You should now have access to any branch of the SPIIR pipeline using the {\it load\_spiir <branch>} bash function and you can navigate to physical repo using the {cd\_spiir <branch>} bash function. This will give you access to the codebase and will set your environment variables necessary to interact with the respective SPIIR branch. \\

\begin{tcolorbox}[colback=yellow!10!white ,
colframe=white!20!black]
\begin{center}
    ------------------------------------------------------
    
    DO NOT \\ CHANGE THE SHARED BRANCH SPECIFIC CODE
    
    ------------------------------------------------------
\end{center}

\end{tcolorbox}

We recommend a feature-branch like workflow with the assumed HEAD->master being the SPIIR master branch. We present a quick step-by-step guide below but please read \ref{sec:gitworkflow} for a more detailed breakdown.  \\

\begin{tcolorbox}[colback=yellow!10!white ,
colframe=white!20!black]
\begin{center}
    ------------------------------------------------------
    
    QUICK GUIDE \\ TO FEATURE-BRANCH LIKE WORKFLOW
    
    ------------------------------------------------------
\end{center}


{\bf1.  Identify/Scope the single feature you'd like to work on}

\indent - think of a single JIRA issue that could be part of a larger epic \\
    
{\bf2. Have a single sentence description} 

\indent - this will be the first line of your future commit message \\
    
{\bf3. Identify the SPIIR function/s you will be changing}
 
\indent - work through the call graph and understand the scope of your changes. If no call graph exists, build one first. \\


    
{\bf4. Create a new branch in GitLab which references <your name> and <feature name> }

\indent - Ideally, your feature branch should not be more than a month long unit of work. If it's larger, you should consider breaking down the work in smaller features with their own branch workflows. THE GOAL IS TO MAKE LSC REVIEWS STRAIGHT FORWARD WITH EACH NEW COMMIT REFLECTING A STAND ALONE FEATURE THAT'S EASY TO DISCUSS. \\
    

{\bf5. Build your new branch}

\indent - navigate to the location of {\it build\_spiir.sh} in your compute environment and simply run \\

\hspace{2cm} {\it bash build\_spiir.sh build\_spiir <your branch>} \\


{\bf6. Write a test for the code you are changing.}
- example test/s are available \href{https://git.ligo.org/alex.codoreanu/gwdc_utils/-/blob/master/unit_testing/create.py}{here.} \\

{\bf7. Develop your feature} \\

{\bf8. Test your feature code and iterate if necessary} \\

{\bf9. Rebase your code}

\indent Once your code passes your tests in the feature env, you must ensure that possible changes\footnote{by other developers working on other features} to the {\it root branch} you've branched from could introduce errors in your current branch. \\

{\bf10. Rerun test/s which passed before and iterate if necessary}\\

{\bf11. Commit and push your changes to {\it <your branch>} head.} \\

{\bf12. On GitLab, request a merge of {\it <your branch>} to {\it <root>} you originally branched off of.}

    
\end{tcolorbox}

\subsection{Python 3 and the SPIIR pipeline}
Please see Section~\ref{ssec:p2to3} for a detailed description of the plan for
transitioning from Python~2 to Python~3.

If you need to work with Python~3, the build script has two commands that will
be of use to you. Running {\it load\_spiir} loads Python~2 libraries and (where
applicable) ensures that the version of Python in your \texttt{\$PATH} is
Python~2. The command:\\

{\bf spiir\_python2to3}
\\

will prepend Python~3 libraries (compiled automatically by the build script as
it creates dependencies) to your environment. On OzStar, it will also unload
Python~2 and load Python~3 and associated modules.

There is an inverse command, {\it spiir\_python3to2}, that you can use to
restore the environment to how it was after running {\it load\_spiir}.
