# FOR OzStar/CiT users:
# in order to add any dependencies associated with the gwdc_utils folder simply add the following:
#
#. <remote path to file>/add_gwdc_utils.sh
# 
# to your .bashrc.
#
# DON'T FORGET THE . OPERATOR FOR ABOVE LINE (it will source the .sh file and provide you with the add_gwdc_utils system function)
#
# and then paste the following inside the same .bashrc file.
#
# add_gwdc_utils
#
# after you load_spiir <branch-name>
#
#
# Your .bashrc should have the following sequence:
#
# load_spiir <branch-name>
# add_gwdc_utils


export GBP_HOSTNAME=`hostname`

add_gwdc_utils() {
        if [[ $GBP_HOSTNAME = farnarkle* ]] || [[ $GBP_HOSTNAME = data-mover* ]] || [[ $GBP_HOSTNAME = john* ]] || [[ $GBP_HOSTNAME = bryan* ]] ;
        then
                module load pandas/0.22.0-python-2.7.14
                export ozstar_utilities_path='/fred/oz016/gwdc_spiir_pipeline_codebase/gwdc_utils'

                export PYTHONPATH=$PYTHONPATH:$ozstar_utilities_path
                export PYTHONPATH=$PYTHONPATH:$ozstar_utilities_path'/dependencies'
        else
                export PYTHONPATH=$PYTHONPATH:$(realpath gwdc_utils)
                export PYTHONPATH=$PYTHONPATH:$(realpath gwdc_utils)'/dependencies'
        fi

}

