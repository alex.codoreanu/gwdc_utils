#!/bin/bash
SUB=1

IFOS="H1 L1 V1"
NEGLAT=30
SBNK="ER14a"
BOPTPERC=97
SUF="b0optmin${BOPTPERC}pc_${SBNK}_${NEGLAT}"

mkdir -p logs_${SUF}
mkdir -p gstlal_iir_bank_${SUF}
for IFO in ${IFOS}; do
  if [ $SUB -eq 1 ]
  then
    sbatch --output=logs_${SUF}/gstlal_iir_bank_%A_${IFO}_%a.out --error=logs_${SUF}/gstlal_iir_bank_%A_${IFO}_%a.err banks.sh ${IFO} ${SUF} ${SUB} ${NEGLAT} ${SBNK} ${BOPTPERC}
  else
    ./banks.sh ${IFO} ${SUF} ${SUB} ${NEGLAT} ${SBNK} ${BOPTPERC}
  fi
done

