from postprocess_utils import create

xml_id = '*zerolag*xml.gz'
setup_file_name = 'postcoh_setup'

run_directory = '/fred/oz996/Manoj/O3b_EW'
sub_folders = ['EW_10_10d', 'EW_15_10d']
folder_indeces = list(range(1, 17))

for folder in sub_folders:
    for folder_index in folder_indeces:
        folder_index_string = '00000{}'.format(folder_index)
        output_file_name = '{}_{}_{}_summary.csv'.format(
            setup_file_name, folder, folder_index_string)

        print output_file_name

        output_path = '{}/{}/{}'.format(
            run_directory, folder,
            folder_index_string[len(folder_index_string) - 3:])

        create.combined_dataframe_from_xml_files(
            output_path,
            xml_id,
            setup_file_name=setup_file_name,
            output_file_name=output_file_name)
