from pandas import read_csv
from plot_utils.create import scatter_plot

setup_file_name = 'postcoh_setup'
sub_folders = ['EW_10_10d', 'EW_15_10d']
folder_indeces = list(range(1, 17))

for folder in sub_folders:
    for folder_index in folder_indeces:
        folder_index_string = '00000{}'.format(folder_index)
        summary_file_name = '{}_{}_{}_summary.csv'.format(
            setup_file_name, folder, folder_index_string)

        summary_file = read_csv(summary_file_name)

        # combinedchi2 vs cohsnr
        plot_name = 'plots/{}'.format(
            summary_file_name.replace('summary.csv', 'coshnr_vs_chi2.jpg'))
        x_data = summary_file.cohsnr
        y_data = summary_file.cmbchisq
        ylabel = 'combined Chi$^2$'
        xlabel = 'cohsnr'
        scatter_plot(x_data,
                     y_data,
                     plot_name,
                     xlabel=xlabel,
                     ylabel=ylabel,
                     ylog=True)

        # mass1 vs mass2 plot
        plot_name = 'plots/{}'.format(
            summary_file_name.replace('summary.csv', 'm1_vs_m2.jpg'))
        xlabel = 'mass$_1$'
        ylabel = 'mass$_2$'
        x_data = summary_file.mass1
        y_data = summary_file.mass2
        scatter_plot(x_data, y_data, plot_name, xlabel=xlabel, ylabel=ylabel)
