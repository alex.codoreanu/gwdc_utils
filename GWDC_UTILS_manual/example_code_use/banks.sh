#!/bin/bash
#
#SBATCH --ntasks=1
#SBATCH --time=102:00:00
#SBATCH --mem-per-cpu=4g
#
#SBATCH --array=0-11
### WARNING: need to specify which banks to run (above)

IFO=$1
SUF=$2
SUB=$3
NEGLAT=$4
SBNK=$5
BOPTPERC=$6
PSD="/fred/oz016/manoj/O3_Banks/H1L1V1-REFERENCE_PSD-1263344418-21600.xml.gz"

### WARNING: check consistency with ../split/${SBNK}/info
# bns 0000-0089
# bns 0090-0099
# nsbh 0100-0111
# nsbh 0112-0399
# bbh 0400-0439

### WARNING: number below should be last BNS bank
cmd () {
  if [ "$BANK" -le 89 ]; then
    APPROX="SpinTaylorT4"
  elif [ "$BANK" -le 99 ]; then
    APPROX="SEOBNRv4_ROM"
  elif [ "$BANK" -le 111 ]; then
    APPROX="SpinTaylorT4"
  else
    APPROX="SEOBNRv4_ROM"
  fi

### DOCUMENTATION FOR OPTIONS:
# epsilon specifies allowed error in phase when placing filters
# epsilon-start gives starting epsilon, which is then adjusted to satisfy various requirements below
# epsilon-{min,max} gives bounds on epsilon
# epsilon-factor specifies how much to adjust epsilon (should be in (1,2])
# filters-{min,max} gives bounds on filter numbers
# filters-per-loglen-{min,max} gives bounds on filters/log2(template_length)
# initial-overlap-{min,max} specfies the bounds on overlap we require (before any optimization)
# b0-optimized-overlap-{min,max} specifies the bounds on overlap we require (after b0 optmization)
# final-overlap-{min,max} specifies the bounds on overlap we require (after a1 optimization)
# nround-max specifices the maximum number of times to adjust epsilon

#if final-overlap-min is not specified, the optimizer will be run after the final value of epsilon is chosen
#if b0-optimized-overlap-min is not specified, the b0 optimizer will be run after final value of epsilon is chosen
  CMD="gstlal_iir_bank --reference-psd ${PSD} --template-bank /fred/oz016/manoj/test/new_split/${SBNK}/${IFO}_split_bank/${IFO}-GSTLAL_SPLIT_BANK_${BNK}-0-0.xml.gz --flow 15.0 --waveform-domain FD --padding 1.3 --instrument ${IFO} --output gstlal_iir_bank_${SUF}/iir_${IFO}-GSTLAL_SPLIT_BANK_${BNK}-a1-0-0.xml.gz --autocorrelation-length 351 --sampleRate 2048.0 -v --epsilon-options "'{"epsilon_start":1.0,"nround_max":25,"initial_overlap_min":0.95,"b0_optimized_overlap_min":0.'"${BOPTPERC}"',"epsilon_factor":1.2,"filters_max":350}'" --optimizer-options "'{"verbose":true,"passes":16,"indv":true,"hessian":true}'" --approximant ${APPROX} --negative-latency ${NEGLAT}"
}

if [ $SUB -eq 1 ]
then
  BANK=$SLURM_ARRAY_TASK_ID
  BNK=$( printf "%04d" $SLURM_ARRAY_TASK_ID)
  cmd
  srun $CMD
else
  for TID in $(seq $(grep -m 1 "SBATCH --array" banks.sh | sed 's/.*=//' | sed 's/-/\ /')); do
    BANK=$TID
    BNK=$( printf "%04d" ${TID})
    cmd
    $CMD > logs_${SUF}/gstlal_iir_bank_${IFO}_${TID}.out 2>logs_${SUF}/gstlal_iir_bank_${IFO}_${TID}.err &
    disown
  done
fi
