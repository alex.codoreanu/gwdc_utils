def clean_gracedb_row(gracedb_row, poptypes_list=None):
    """This function cleans up a client dictionary by poping any
    key where type(dict[key]) is in poptypes_list. If no poptypes_list
    is specified then poptypes_list = [list, dict].

    :param gracedb_row: an event object from client.events()
    :return: DataFrame object with now [list, dict] items
    """
