def get_current_time_string():
    """This function returns a time string where the datetime output
    has underscores instead of local_el in ['-', ' ', ':', '.'].

    :return current time string:
    """
