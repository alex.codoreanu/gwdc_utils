def get_GraceDB_client(playground=False):
    """This function returns a connection to GraceDB.

    :param playground: if True the returned connection
                       will point to GraceDB-playground

    :return a connection to GraceDB
    """
