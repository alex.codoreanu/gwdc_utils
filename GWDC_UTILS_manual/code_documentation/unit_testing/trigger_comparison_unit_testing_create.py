def trigger_comparison(verbose=True, dependencies_dir=None, offline_IFOs=None):
    """This test runs a comparison on the output of a newly run offline trigger.

    It runs a new trigger and then compares the resulting single snr series for each detector with an already
    created reference file. For further details see JIRA ticket SPIIP-76.

    Details are logged in an output file whose name depends on the time at which the code was run.
    It logs:
        - location of unit_testing module
        - the machine on which the code ran
        - all PYTHONPATH sub-paths
        - possible reasons for failure

    USAGE:
    from unit_testing.create import trigger_comparison
    result = trigger_comparison()

    @param verbose: bool to display status to device
    @param dependencies_dir: location of dependencies
    @param offline_IFOs: list of offline instruments.
                         Current available IFOs list is: ['Livingston', 'Hanford', 'Virgo']


    @return: return_bool: bool result of test
    """
