def bank_generation_comparison(ifo='H1', latency=0, spiir_branch='master'):
    """This function writes and submits a SLURM job which first creates/compares a new comparison bank with an existing
    reference bank. The job runs the unit_testing/dependencies/bank_generation/build_and_compare_new_bank.py file.

    The individual templates (rows) of this comparison bank are compared one by one with each respective individual
    templates from the corresponding reference bank. The result of each template comparison is logged. This log is then
    used to identify if the test passed or failed. ALL intermediary files are written to the user's HOME directory.

    The intermediary files are:
        - the SLURM submission file: bg_test_<ifo>_<latency>.sh
        - associated .out and .err files are both of format: bg_unit_test_IFO_<ifo>_latency_<latency>.out/err
        - comparison bank: <ifo>-GSTLAL_SPLIT_BANK_0003-0-0-latency_<latency>_<spiir_branch>.xml.gz
        - log of each template comparison: bg_comparison_IFO_<ifo>_latency_<latency>_<spiir_branch>.csv
        - pass/fail log: bg_comparison_IFO_<ifo>_latency_<latency>_<spiir_branch>_<PASS/FAIL>.txt

    USAGE:
        from unit_testing.create import bank_generation_comparison
        bank_generation_comparison(ifo='L1', latency=10, spiir_branch='andrew_bank_acceleration')

    @param ifo: instrument identifier; current options are ['L1', 'V1', 'H1']
    @param latency: the latency of the bank; current options are [0, 10, 20, 30]
    @param spiir_branch: the spiir branch you wish to use for the bank generation regression test

    @return:
    """
