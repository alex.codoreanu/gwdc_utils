def images_similar(f1, f2, threshold=5):
    """Use the Imagemagick 'compare' utility to check if two images are vaguely
    similar. The threshold is based on IM's 'AE' (absolute error) metric and
    for now is a guess.

    @param f1: name of image to compare
    @param f2: name of image to compare
    @param threshold: reference threshold
    """
