def get_user(temp_path):
    """Get user of folder: temp_path.

    @param temp_path: target directory
    @return: user
    """
