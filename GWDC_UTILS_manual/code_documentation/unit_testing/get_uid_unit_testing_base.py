def get_uid(temp_path):
    """Get uid of folder: temp_path.

    @param temp_path: target directory
    @return: uid
    """
