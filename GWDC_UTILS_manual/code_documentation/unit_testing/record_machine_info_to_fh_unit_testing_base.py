def record_machine_info_to_fh(logfile_fh, verbose):
    """Record common information to the given logfile file handle.

    This common information is e.g. the machine being run on, location of test
    code, environment variables, and other things that may affect test
    execution

    @param logfile_fh: file handle
    @param verbose: whether to print to stdout as well
    """
