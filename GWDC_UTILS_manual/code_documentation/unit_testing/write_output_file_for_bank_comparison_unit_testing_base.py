def write_output_file_for_bank_comparison(log_file_name,
                                          number_of_allowed_bad_templates, IFO,
                                          latency, spiir_branch):
    """This function reads in a log file written by unit_testing.base.compare_full_banks() and writes the final output
    file for the unit_testing.create.bank_generation_comparison() function.

    @param log_file_name:
    @param number_of_allowed_bad_templates:
    @param IFO:
    @param latency:
    @param spiir_branch:
    @return:
    """
