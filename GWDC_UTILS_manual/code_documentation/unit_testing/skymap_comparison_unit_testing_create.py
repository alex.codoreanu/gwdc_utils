def skymap_comparison(verbose=True):
    """Test to see if skymap conversion, from raw form to PNG via FITS, is
    working. The test case is data from a run on GW170817.

    The comparison at the end is tricky because we need to compare images, but
    would prefer not to fail just because a slightly different font is
    installed. We use ImageMagick's 'compare' command, although this may need
    to be revisited in future

    USAGE
    from unit_testing.create import skymap_comparison
    result = skymap_comparison()

    @param verbose: be very chatty when making things happen

    @return: return_bool: result of the test
    """
