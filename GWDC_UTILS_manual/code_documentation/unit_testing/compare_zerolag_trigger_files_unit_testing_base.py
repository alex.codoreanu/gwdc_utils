def compare_zerolag_trigger_files(comparison_file,
                                  reference_file=None,
                                  reference_keys=None,
                                  threshold_value=None,
                                  offline_detector_ids=None):
    """

    USAGE:
    from unit_testing.base import compare_zerolag_trigger_files

    # compare file to itself
    comparison_file = 'postcoh__summary.csv'

    if compare_zerolag_trigger_files(comparison_file):
        'compare zerolag files test: PASS'
    else:
        'compare zerolag files test: FAIL'


    @param offline_detector_ids:
    @return:
    @param comparison_file: new file to be compared with reference file
    @param reference_file: reference file to be used for comparison. Description of default file
                           is available in associated JIRA ticket SPIIP-76.
    @param reference_keys:
    @param threshold_value:
    @return: bool

    """
