def get_group(temp_path):
    """Get group of folder: temp_path.

    @param temp_path: target directory
    @return: group
    """
