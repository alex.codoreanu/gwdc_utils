def get_gid(temp_path):
    """Get gid of folder: temp_path.

    @param temp_path: target directory
    @return: gid
    """
