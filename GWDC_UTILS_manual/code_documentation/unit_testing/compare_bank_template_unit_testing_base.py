def compare_bank_template(reference_bank,
                          comparison_bank,
                          template_number,
                          pct_threshold=None):
    """This function compares a template row between two banks. It extracts the ['a1', 'b0', 'd'] parameters and then
    calls compare_template_parameters() which returns a bool.

    @param reference_bank:
    @param comparison_bank:
    @param template_number:
    @param pct_threshold:
    @return: check_bool
    """
