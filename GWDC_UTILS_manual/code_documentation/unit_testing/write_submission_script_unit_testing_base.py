def write_submission_script(ifo, latency, spiir_branch='master'):
    """This function writes a SLURM submission script for the bank generation regression test:
    unit_testing/create.bank_generation_comparison()

    @param ifo: instrument string identifier
    @param latency: integer latency value
    @param spiir_branch: string identifier of ALREADY built branch to be tested

    @return:
    """
