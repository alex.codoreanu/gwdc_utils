def compare_full_banks(reference_bank_name,
                       comparison_bank_name,
                       log_file_name,
                       verbose=True):
    """This function is a wrapper for unit_testing.base.compare_bank_template() to compare two full banks and log the
    result of each comparison.

    @param reference_bank_name:
    @param comparison_bank_name:
    @param log_file_name:
    @param verbose:
    @return:
    """
