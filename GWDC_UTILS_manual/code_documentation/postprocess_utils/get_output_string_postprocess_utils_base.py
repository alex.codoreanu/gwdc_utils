def get_output_string(local_temp_line,
                      temp_element,
                      variable_labels,
                      cleanup_elements=None):
    """This function returns a comma separated line with the extracted values for all
    present variable_labels in local_temp_line.

    @param local_temp_line: line to be considered
    @param temp_element: element to be deleted from local_temp_line.
                         To be passed to get_line_elements().
    @param variable_labels: the variables to be searched for.
    @param cleanup_elements: elements to be cleaned up from local_temp_line.
                             Default items are ['\n', '=', ')'].

    @return: a comma separated line containing the values in local_temp_line
    """
