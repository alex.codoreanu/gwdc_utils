def combined_dataframe_from_xml_files(xml_path,
                                      xml_id,
                                      setup_file_name=None,
                                      output_file_path=None,
                                      output_file_name=None,
                                      verbose=False,
                                      offline_detector_ids=None):
    """This function identifies all files containing xml_id in directory xml_path.
    It then uses the setup_file_name .csv to identify all TABLE/PARAMETER combinations and
    create a combined __summary.csv file.

    EXAMPLE USAGE:
        from postprocess_utils import create

        xml_path = '/fred/oz016/anwarul/Rerun_devOct17_Feb28/inj/018'
        xml_id = '*zerolag*xml.gz'
        setup_file_name = 'postcoh_setup'

        create.combined_dataframe_from_xml_files(xml_path, xml_id, setup_file_name=setup_file_name, verbose=True)

    @param xml_path: the directory holding the .xml files
    @param xml_id: the substring which identifies the relevant .xml files
    @param setup_file_name: the setup file name which identifies which table and
                            which parameters you wish to extract
    @param output_file_path: the output path for the __summary.csv file
    @param output_file_name: specific file name for the __summary.csv file
    @param offline_detector_ids: list of string identifiers for offline detector related variables
    @param verbose: a BOOL which denotes if you wish to have verbose output during the run of the function

    @return:
    """
