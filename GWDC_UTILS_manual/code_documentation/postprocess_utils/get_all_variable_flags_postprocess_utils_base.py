def get_all_variable_flags(all_temp_lines, line_ids=None, verbose=False):
    """This function returns a list of all possible variables present in the whole
    collection in all_temp_lines. These will be the header when extracting the variables
    themselves later on.

    @param all_temp_lines: I/O object containig all lines in question
    @param line_ids: list containing string identifiers to be looked for
    @param verbose: bool to print status

    @return: list of all present labels as identified by the '=' symbol
    """
