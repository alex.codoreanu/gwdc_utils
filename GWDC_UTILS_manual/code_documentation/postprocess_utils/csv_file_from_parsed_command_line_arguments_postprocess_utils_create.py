def csv_file_from_parsed_command_line_arguments(reference_file_name,
                                                output_file_name,
                                                default_line_ids=None,
                                                verbose=True):
    """This function creates a .csv data structure comprised of all identified parser.add_option
    flags in reference_file_name.

    USAGE:

        reference_file_name = 'original_parse_arguments.py'
        output_file_name = 'parsed_arguments.csv'

        from postprocess_utils.create import csv_file_from_parsed_command_line_arguments

        csv_file_from_parsed_command_line_arguments(reference_file_name, output_file_name)


    @param reference_file_name: file to be investigated
    @param output_file_name: output csv file
    @param default_line_ids: list containing elements
                             which can identify a line of interest
    @param verbose: bool to track progress

    @return:
    """
