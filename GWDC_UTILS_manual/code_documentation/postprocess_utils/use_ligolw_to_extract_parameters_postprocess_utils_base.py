def use_ligolw_to_extract_parameters(local_table_name=None,
                                     local_parameters=None,
                                     file_name=None,
                                     local_output_file_name=None,
                                     print_exec_line=False):
    """This function is a wrapper for the ligolw_print system function that
    appends to a local_output_file_name .csv file with
        COLUMNS:local_parameters extracted from
        TABLE:local_table_name.

    :param local_table_name: the table name from which to extract relevant parameter values
    :param local_parameters: the parameter columns which are to be extracted
    :param file_name: the trigger file name from which to extract from
    :param local_output_file_name: where to output the reshaped table data
    :param print_exec_line: BOOL which prints the ligolw_print line

    :return:
    """
