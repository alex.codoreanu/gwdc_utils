def create_err_status_file(log_directory,
                           dir_path,
                           overwrite_original=True,
                           err_out_lines=False):
    # type: (object, object, object, object) -> object
    """This function creates a single status file associated with each .err file in the log directory in dir_path.
    This file holds all of the relevant single line entries from the .err files (ie. SBANK, NEGLAT, m1, m2, ...)

    @param log_directory:
    @param dir_path:
    @param overwrite_original:
    @param err_out_lines:

    @return: 0
    """
