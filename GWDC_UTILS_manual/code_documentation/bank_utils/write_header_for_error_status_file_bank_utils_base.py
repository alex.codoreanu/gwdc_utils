def write_header_for_error_status_file(log_directory,
                                       overwrite=False,
                                       header_line=None):
    """This function will take in the log directory and create a status file to where all of the
    relevant lines will be appended to.

    :param log_directory: the relevant log directory
    :param overwrite: a BOOL which will define if an existing file should be overwritten (ie. the function has already
    been run in the past but maybe failed for some reason)
    :param header_line: the default header line. This should only be changed if the submission scripts change.

    :return: output_file: the name of the *_err_status.csv file
    """
