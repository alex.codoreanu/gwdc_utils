def get_single_template_parameters_from_bank_object(bank_object, template_id):
    """ This function extracts the rate and real components of the A, B, and D components
    of a template(template_id) from a bank_object.

    @param bank_object:
    @param tmplt_id:
    @return: rate, realA, realB, realD
    """
