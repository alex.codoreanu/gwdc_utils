def compare_template_parameters(reference_items=None,
                                comparison_items=None,
                                reference_names=None,
                                pct_theshold=None):
    """ This function computes the relative difference between each sequncial and respective elements of
    reference_items and comparison_items.

    Returns TRUE if relative difference is below or equal to pct_theshold.

    @param reference_items: list of lists containing baseline elements
    @param comparison_items: list of lists containing comparison elements
    @param reference_names: list of strings identifiers corresponding to baseline/comparison elements
    @param pct_theshold: percentage threshold to be applied to each item comparison. Default value is 5%

    @return: check_bool: True if relative difference for each item is below pct_threshold
    """
