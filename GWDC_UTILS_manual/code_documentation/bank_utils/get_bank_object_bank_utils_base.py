def get_bank_object(bank_name=None):
    """This function returns a bank object.
    
    @param bank_name: name with full path of the bank to be read in

    @return: bank object
    """
