def parse_relevant_err_file_line(line,
                                 job_id,
                                 IFO,
                                 BANK,
                                 BOPTERC,
                                 SBANK,
                                 NEGLAT,
                                 err_out=True):
    """This function parses a passed line and identifies the relevant variables and puts them together in the same order
    as passed in the header line.

    :param line: input line to be parsed
    :param job_id: SLURM job id

    THE BELOW ARE JOB SUBMISSION RELEVANT SCRIPTS THAT HAVE BEEN PULLED LOWER DOWN THE CALL TREE.
    :param IFO:
    :param BANK:
    :param BOPTERC:
    :param SBANK:
    :param NEGLAT:

    :param err_out: a BOOL value that decides whether or not to fail in the case of a parse error.

    :return: output line: comma separated line with the same order of items as passed to the header
    """
