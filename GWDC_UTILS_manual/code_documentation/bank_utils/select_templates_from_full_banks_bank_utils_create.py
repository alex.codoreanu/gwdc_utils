def select_templates_from_full_banks(reference_psd,
                                     template_bank_name,
                                     output_file_name,
                                     bank_id,
                                     negative_latency=0,
                                     templates=None,
                                     bank_generation_setup_file=None):
    """This function is a more verbose Python version of gstlal_iir_bank. This function is written as to provide a
    single scope in which to test and investigate the performance of the bank generation script.

    The original script was originally written by:
    # Copyright (C) 2011-2012  Shaun Hooper
    # Copyright (C) 2013-2015  Qi Chu

    Additional functionality offered by this wrapper includes:
        - the ability to only create specific templates from the selected Bank using the templates variable
        - the ability to specify if you want to overwrite an existing file with the same name. If not, it will
            continously rename the file until a new version is selected.
            This will ensure that you won't accidentally overwrite files if you forget to check the paths.
        - the ability to keep track of the last template started by using the keep_track_bool boolean. The current
        template number is passed to a file in the same directory as the output.

    The input variables are kept consistent with the original code and can be pulled from the original gstlal_iir_bank
    documentation.

    @param reference_psd:
    @param template_bank_name:
    @param output_file_name:
    @param bank_id:
    @param negative_latency:
    @param templates:
    @param bank_generation_setup_file:

    @return:
    """
