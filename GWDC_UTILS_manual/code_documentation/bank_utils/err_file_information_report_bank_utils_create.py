def err_file_information_report(dir_path, check_logs_string='logs_*'):
    """This is the main function wrapper for creating a series of status report files.
    An example use case can be seen in:
    example_create_dot_err_file_information_report_run.txt

    usage:
        from bank_utils.create import err_file_information_report
        err_file_information_report('/fred/oz016/PSD-1263344418-21600')


    :param dir_path: The directory in which the templates are located
    :param check_logs_string: A string indentifier to find the directories holding the .err files associated with the
    bank generation scripts.
    :return: 0
    """
