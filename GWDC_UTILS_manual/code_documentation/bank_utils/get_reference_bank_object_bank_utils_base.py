def get_reference_bank_object(IFO=None, negative_latency=None):
    """This function loads a reference bank object for the bank generation unit test.

    @param IFO: instrument short name such as a single element from: ['L1', 'V1', 'H1']
    @param negative_latency: latency value to be used.
                             Current options: [0, 10, 20, 30]
    @return: reference bank object
    """
