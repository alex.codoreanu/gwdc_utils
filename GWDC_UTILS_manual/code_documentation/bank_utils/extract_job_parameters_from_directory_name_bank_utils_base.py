def extract_job_parameters_from_directory_name(log_directory, dir_path):
    """This function will extract the job submission parameters from the output directory name.
    The variable names correspond 1to1 with the variable names used in the job submission scripts.
    These variables are:

    BOPTERC
    SBANK
    NEGLAT
    SUF

    :param log_directory: the identified log directory
    :param dir_path: the path to the identified log directory. This is the location of the template files
    :return: BOPTPERC, SBANK, NEGLAT, SUF
    """
