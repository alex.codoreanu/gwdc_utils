def make_dictionary(state_file_name=None):
    """This function is designed to create a multi-data type dictionary from a ';' separated file.
    The expected header of the file is:
        variable;value;type
    state_file_name = '/fred/oz996/alex_dev/gwdc_utils/bank_utils/default_bank_generation_setup.csv'

    Current supported types are:
        allowed_state_data_types = ['int', 'string', 'float', 'bool', 'dict']

    The purpose of this function is to update the running state of a python environment. Works a bit like a
    config file but does not have to have each variable extracted+assigned within scope. The same 3 lines work
    (4 lines including the import statement) if you have 5 or 50 variables to load into scope.

    USAGE:
        from bank_utils.base import make_dictionary

        local_dict = make_dictionary()
        for key, value in local_dict.items():
            vars()[key] = value

    @param state_file_name: the setup file containing the variables with default values and types you
                            wish to load into scope

    @return:local_dict: a dictionary built from the entries in state_file_name
    """
