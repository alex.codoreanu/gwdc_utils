def scatter_plot(x_data,
                 y_data,
                 plot_name,
                 xlabel=None,
                 ylabel=None,
                 xlog=False,
                 ylog=False,
                 background=True,
                 grid=True,
                 title=True,
                 verbose=True):
    """This is a generic scatter plot function which
     has access to preloaded plot style parameters from base.py.

    :param x_data: x_data vector
    :param y_data: y_data vector
    :param plot_name: the full name with path of the plot to be saved
    :param xlabel: xlabel string
    :param ylabel: ylabel string
    :param xlog: if 'True' then axis will be in log-scale
    :param ylog: if 'True' then axis will be in log-scale
    :param background: if 'True' then background will be base.background_color
    :param grid: if 'True' then grid lines will be displayed
    :param title: if 'True' then a title will be extracted from plot_name
                  from the last '/' to the last '.' character.
    :param verbose: if 'True' then print statements will be displayed
    :return:
    """
