\contentsline {section}{\numberline {1}Introduction}{4}{section.1}% 
\contentsline {section}{\numberline {2}Code Style Guide}{5}{section.2}% 
\contentsline {section}{\numberline {3}Use Cases}{6}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Bank generation}{6}{subsection.3.1}% 
\contentsline {subsubsection}{\numberline {3.1.1}submit\_30.sh}{6}{subsubsection.3.1.1}% 
\contentsline {subsubsection}{\numberline {3.1.2}bank\_postprocessing example}{6}{subsubsection.3.1.2}% 
\contentsline {subsubsection}{\numberline {3.1.3}TO DO items}{7}{subsubsection.3.1.3}% 
\contentsline {subsection}{\numberline {3.2}Injection run follow-up}{8}{subsection.3.2}% 
\contentsline {subsubsection}{\numberline {3.2.1}combined\_dataframe\_from\_xml\_files\_\_test.py}{8}{subsubsection.3.2.1}% 
\contentsline {subsubsection}{\numberline {3.2.2}create\_scatter\_plot\_\_test.py}{9}{subsubsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.3}Interacting with databases}{9}{subsection.3.3}% 
\contentsline {subsubsection}{\numberline {3.3.1}Gracedb}{9}{subsubsection.3.3.1}% 
\contentsline {subsubsection}{\numberline {3.3.2}GwdcDb}{9}{subsubsection.3.3.2}% 
\contentsline {paragraph}{Test Connection:}{9}{section*.2}% 
\contentsline {paragraph}{Working with Pandas:}{11}{section*.3}% 
\contentsline {section}{\numberline {4}Regression tests}{12}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Source detection:\newline unit\_testing.create.trigger\_comparison()}{12}{subsection.4.1}% 
\contentsline {subsection}{\numberline {4.2}Source localisation:\newline unit\_testing.create.skymap\_comparison()}{12}{subsection.4.2}% 
\contentsline {subsection}{\numberline {4.3}Template bank generation: \newline unit\_testing.create.bank\_generation\_comparison()}{12}{subsection.4.3}% 
\contentsline {section}{\numberline {A}bank\_utils}{14}{appendix.A}% 
\contentsline {subsection}{\numberline {A.1}create.py}{14}{subsection.A.1}% 
\contentsline {subsubsection}{\numberline {A.1.1}err\_file\_information\_report}{14}{subsubsection.A.1.1}% 
\contentsline {subsubsection}{\numberline {A.1.2}select\_templates\_from\_full\_banks}{14}{subsubsection.A.1.2}% 
\contentsline {subsection}{\numberline {A.2}base.py}{15}{subsection.A.2}% 
\contentsline {subsubsection}{\numberline {A.2.1}get\_approximant}{15}{subsubsection.A.2.1}% 
\contentsline {subsubsection}{\numberline {A.2.2}get\_num\_jobs}{15}{subsubsection.A.2.2}% 
\contentsline {subsubsection}{\numberline {A.2.3}extract\_job\_parameters\_from\_directory\_name}{15}{subsubsection.A.2.3}% 
\contentsline {subsubsection}{\numberline {A.2.4}write\_header\_for\_error\_status\_file}{15}{subsubsection.A.2.4}% 
\contentsline {subsubsection}{\numberline {A.2.5}parse\_relevant\_err\_file\_line}{16}{subsubsection.A.2.5}% 
\contentsline {subsubsection}{\numberline {A.2.6}create\_err\_status\_file}{16}{subsubsection.A.2.6}% 
\contentsline {subsubsection}{\numberline {A.2.7}create\_single\_bank\_report\_file}{16}{subsubsection.A.2.7}% 
\contentsline {subsubsection}{\numberline {A.2.8}get\_bank\_object}{17}{subsubsection.A.2.8}% 
\contentsline {subsubsection}{\numberline {A.2.9}get\_single\_template\_parameters\_from\_bank\_object}{17}{subsubsection.A.2.9}% 
\contentsline {subsubsection}{\numberline {A.2.10}compare\_template\_parameters}{17}{subsubsection.A.2.10}% 
\contentsline {subsubsection}{\numberline {A.2.11}get\_reference\_bank\_object}{17}{subsubsection.A.2.11}% 
\contentsline {subsubsection}{\numberline {A.2.12}make\_dictionary}{18}{subsubsection.A.2.12}% 
\contentsline {section}{\numberline {B}postprocess\_utils}{18}{appendix.B}% 
\contentsline {subsection}{\numberline {B.1}create.py}{18}{subsection.B.1}% 
\contentsline {subsubsection}{\numberline {B.1.1}combined\_dataframe\_from\_xml\_files}{18}{subsubsection.B.1.1}% 
\contentsline {subsubsection}{\numberline {B.1.2}csv\_file\_from\_parsed\_command\_line\_arguments}{19}{subsubsection.B.1.2}% 
\contentsline {subsection}{\numberline {B.2}base.py}{19}{subsection.B.2}% 
\contentsline {subsubsection}{\numberline {B.2.1}use\_ligolw\_to\_extract\_parameters}{19}{subsubsection.B.2.1}% 
\contentsline {subsubsection}{\numberline {B.2.2}get\_line\_elements}{19}{subsubsection.B.2.2}% 
\contentsline {subsubsection}{\numberline {B.2.3}get\_all\_variable\_flags}{20}{subsubsection.B.2.3}% 
\contentsline {subsubsection}{\numberline {B.2.4}get\_output\_string}{20}{subsubsection.B.2.4}% 
\contentsline {section}{\numberline {C}db\_utils}{20}{appendix.C}% 
\contentsline {subsection}{\numberline {C.1}base.py}{20}{subsection.C.1}% 
\contentsline {subsubsection}{\numberline {C.1.1}get\_current\_time\_string}{20}{subsubsection.C.1.1}% 
\contentsline {subsubsection}{\numberline {C.1.2}get\_GraceDB\_client}{20}{subsubsection.C.1.2}% 
\contentsline {subsubsection}{\numberline {C.1.3}clean\_gracedb\_row}{20}{subsubsection.C.1.3}% 
\contentsline {subsubsection}{\numberline {C.1.4}get\_event\_df}{21}{subsubsection.C.1.4}% 
\contentsline {subsubsection}{\numberline {C.1.5}get\_autocorr\_and\_latency}{21}{subsubsection.C.1.5}% 
\contentsline {subsection}{\numberline {C.2}GwdcDb.py}{22}{subsection.C.2}% 
\contentsline {section}{\numberline {D}plot\_utils}{22}{appendix.D}% 
\contentsline {subsection}{\numberline {D.1}create.py}{22}{subsection.D.1}% 
\contentsline {subsubsection}{\numberline {D.1.1}heatmap\_plot}{22}{subsubsection.D.1.1}% 
\contentsline {subsubsection}{\numberline {D.1.2}scatter\_plot\_2hist}{22}{subsubsection.D.1.2}% 
\contentsline {subsubsection}{\numberline {D.1.3}scatter\_plot}{23}{subsubsection.D.1.3}% 
\contentsline {subsection}{\numberline {D.2}base.py}{24}{subsection.D.2}% 
\contentsline {section}{\numberline {E}unit\_testing}{24}{appendix.E}% 
\contentsline {subsection}{\numberline {E.1}create.py}{24}{subsection.E.1}% 
\contentsline {subsubsection}{\numberline {E.1.1}trigger\_comparison}{24}{subsubsection.E.1.1}% 
\contentsline {subsubsection}{\numberline {E.1.2}skymap\_comparison}{24}{subsubsection.E.1.2}% 
\contentsline {subsubsection}{\numberline {E.1.3}bank\_generation\_comparison}{24}{subsubsection.E.1.3}% 
\contentsline {subsection}{\numberline {E.2}base.py}{25}{subsection.E.2}% 
\contentsline {subsubsection}{\numberline {E.2.1}append\_line\_to\_file}{25}{subsubsection.E.2.1}% 
\contentsline {subsubsection}{\numberline {E.2.2}append\_line\_to\_fh}{25}{subsubsection.E.2.2}% 
\contentsline {subsubsection}{\numberline {E.2.3}record\_machine\_info\_to\_fh}{25}{subsubsection.E.2.3}% 
\contentsline {subsubsection}{\numberline {E.2.4}images\_similar}{25}{subsubsection.E.2.4}% 
\contentsline {subsubsection}{\numberline {E.2.5}get\_uid}{26}{subsubsection.E.2.5}% 
\contentsline {subsubsection}{\numberline {E.2.6}get\_gid}{26}{subsubsection.E.2.6}% 
\contentsline {subsubsection}{\numberline {E.2.7}get\_user}{26}{subsubsection.E.2.7}% 
\contentsline {subsubsection}{\numberline {E.2.8}get\_group}{26}{subsubsection.E.2.8}% 
\contentsline {subsubsection}{\numberline {E.2.9}get\_current\_time\_string}{26}{subsubsection.E.2.9}% 
\contentsline {subsubsection}{\numberline {E.2.10}compare\_zerolag\_trigger\_files}{26}{subsubsection.E.2.10}% 
\contentsline {subsubsection}{\numberline {E.2.11}write\_submission\_script}{27}{subsubsection.E.2.11}% 
\contentsline {subsubsection}{\numberline {E.2.12}compare\_bank\_template}{27}{subsubsection.E.2.12}% 
\contentsline {subsubsection}{\numberline {E.2.13}compare\_full\_banks}{27}{subsubsection.E.2.13}% 
\contentsline {subsubsection}{\numberline {E.2.14}write\_output\_file\_for\_bank\_comparison}{28}{subsubsection.E.2.14}% 
\contentsline {section}{\numberline {F}banks.sh}{29}{appendix.F}% 
\contentsline {subsubsection}{\numberline {F.0.1}verbose output of create.err\_file\_information\_report}{30}{subsubsection.F.0.1}% 
\contentsline {section}{\numberline {G}create\_docs.py}{30}{appendix.G}% 
