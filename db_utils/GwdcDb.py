import os.path
import sqlalchemy
import urlparse
import six.moves.urllib as urllib


class GwdcDb:
    """Manage connections to the GWDC database

    This class is for managing connections to the GWDC SQL database on OzStar.
    It's designed to auto-configure what it can while still being reasonably
    flexible.

    The SQL Server is a Microsoft SQL Server and thus expects Microsoft's
    dialect of SQL, although you are encouraged to write simple, cross-platform
    SQL where reasonable.

    Usage:
        db = db_utils.GwdcDb()
        with db.get_connection() as conn:
            # do something with the SQLAlchemy connection object 'conn'
    """
    def __init__(self, username='gwdc_admin', password=None):
        """Set up the database connection

        By default, assume the username 'gwdc_admin' and that the password is
        contained in a file called 'gwdc_db_password' in your home directory.
        You can pass a username and password as keyword arguments if you have a
        different one.

        Note that as this is built on SQLAlchemy, we follow their convention of
        lazily making the initial connection -- so the object initialising (or
        even being able to call 'get_engine') is no guarantee that the
        connection was successful.

        :param username: SQL server username, default 'gwdc_admin'
        :param password: SQL server password, default to what's contained in
                         '~/gwdc_db_password'
        """
        if password is None:
            try:
                fh = open(os.path.expanduser('~/gwdc_db_password'))
                password = fh.read()
                fh.close()
                password = password.rstrip("\r\n")
            except:
                raise ValueError("No password specified, and can't read " \
                    + "~/gwdc_db_password, so can't connect to the database")

        self.__import_pyodbc()

        # Make the connection string.
        # We can't in general trust that our ODBC setup will be correctly
        # configured (because we may be using the one in 'dependencies' rather
        # than a system-wide one) so we manufacture the DSN by hand

        # We need to point to:
        # /dependencies/ms-odbc-driver/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.5.so.2.1
        # Importantly, 'msodbcsqlr17.rll' needs to be in
        # '../share/resources/en_US' relative to the .so or you will get an
        # obscure error, IM004.
        dsn = (
            r'Driver=' + os.path.dirname(os.path.abspath(__file__)) + \
            r'/../dependencies/ms-odbc-driver/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.5.so.2.1',
            r'Server=tcp:prd-gwdc1-db.ds.swin.edu.au,7946',
            r'UID=' + username,
            r'PWD=' + password,
        )

        dsn_string = 'mssql+pyodbc:///?odbc_connect=' + \
            urllib.parse.quote_plus(";".join(dsn))

        self.engine = sqlalchemy.create_engine(dsn_string)

    def get_engine(self):
        """Return an SQLAlchemy 'engine' object"""
        return self.engine

    def get_connection(self):
        """Return an SQLAlchemy 'connection' object

        Ideally you should wrap this in a `with` block to ensure that the
        database connection resources are released when you're done with them.
        """
        return self.engine.connect()

    def __import_pyodbc(self):
        """(Internal) Find and load .so containing `pyodbc`

        We can't guarantee LD_LIBRARY_PATH is set to the right place in
        dependencies/, and we can't change LD_LIBRARY_PATH (because the loader
        has already read it in and cached it in memory). This checks if
        'pyodbc' is readable, and if not, runs `LoadLibrary` on the relevant
        shared libraries before reloading it.
        """
        try:
            import pyodbc
        except ImportError as e:
            # try again
            trial_libodbc_path = os.path.dirname(os.path.abspath(__file__))\
                + "/../dependencies/unixODBC/lib/"
            if os.path.exists(trial_libodbc_path + "libodbc.so.2"):
                import ctypes
                ctypes.cdll.LoadLibrary(trial_libodbc_path + "libodbc.so.2")
                ctypes.cdll.LoadLibrary(trial_libodbc_path +
                                        "libodbcinst.so.2")
                import pyodbc  # don't wrap this in try; we can't recover
            else:
                print("Couldn't find libodbc.so.2 :(")
                raise
        # let other exceptions bubble up
