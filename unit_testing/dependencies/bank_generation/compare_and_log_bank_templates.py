import sys

from bank_utils.base import compare_template_parameters
from bank_utils.base import get_bank_object
from bank_utils.base import get_single_template_parameters_from_bank_object
from pandas import read_csv
from unit_testing.base import base_path as unit_test_base_path
from unit_testing.base import get_current_time_string
from unit_testing.base import record_machine_info_to_fh

number_of_allowed_bad_templates = 100
bg_dependencies_path = "{}/dependencies/".format(unit_test_base_path)

IFO = sys.argv[1]
latency = int(sys.argv[2])
spiir_branch = sys.argv[3]

reference_names = ['a1', 'b0', 'd']

log_file_name = 'bg_comparison_IFO_{}_latency_{}_{}.csv'.format(
    IFO, latency, spiir_branch)
print("Logging template comparison results to: {}".format(log_file_name))

with open(log_file_name, 'w') as w:
    w.writelines('template,test_result\n')

comparison_bank_name = "{}-GSTLAL_SPLIT_BANK_0003-0-0-latency_{}_{}.xml.gz\n".format(
    IFO, latency, spiir_branch)
reference_bank_path = "{}/dependencies/bank_generation/reference_output_banks".format(
    unit_test_base_path)
reference_bank_name = "{}/{}-GSTLAL_SPLIT_BANK_0003-0-0-latency_{}.xml.gz\n".format(
    reference_bank_path, IFO, latency)

reference_bank = get_bank_object(reference_bank_name)
comparison_bank = get_bank_object(comparison_bank_name)

print("\nRead in:\n "
      "reference_bank:{}\n "
      "comparison_bank:{}\n".format(reference_bank_name, comparison_bank_name))

templates = list(range(len(comparison_bank.sngl_inspiral_table)))

print("\nStarting to compare {} templates.\n".format(len(templates)))

for template in templates:
    reference_rate, \
    reference_a1, \
    reference_b0, \
    reference_d = get_single_template_parameters_from_bank_object(reference_bank,
                                                                  template)

    comparison_rate, \
    comparison_a1, \
    comparison_b0, \
    comparison_d = get_single_template_parameters_from_bank_object(comparison_bank,
                                                                   template)

    reference_items = [reference_a1, reference_b0, reference_d]
    comparison_items = [comparison_a1, comparison_b0, comparison_d]

    check_bool = compare_template_parameters(reference_items=reference_items,
                                             comparison_items=comparison_items,
                                             reference_names=reference_names,
                                             pct_theshold=None)

    with open(log_file_name, 'a') as a:
        a.writelines("{},{}\n".format(template, check_bool))

    print("Finished template: {}".format(template))

print("Finished all templates\nReading in")

log = read_csv(log_file_name)
print("Read in {}".format(log_file_name))

test_bool = True
number_of_bad_templates = len(log.loc[log.test_result == False].index.tolist())
number_of_good_templates = len(
    log.loc[log.test_result != False].index.tolist())
if number_of_bad_templates > number_of_allowed_bad_templates:
    test_bool = False

if test_bool:
    print("BANK GENERATION REGRESSION TEST PASSED\n")
else:
    print("BANK GENERATION REGRESSION TEST FAILED\n")

log_file_name = 'bg_comparison_IFO_{}_latency_{}_{}.csv'.format(
    IFO, latency, spiir_branch)

bg_output_file = 'bg_comparison_IFO_{}_latency_{}_{}'.format(
    IFO, latency, spiir_branch)
if test_bool:
    bg_output_file = "{}_PASS.txt".format(bg_output_file)
else:
    bg_output_file = "{}_FAIL.txt".format(bg_output_file)

print("writing final log file: {}".format(bg_output_file))

IFO = sys.argv[1]
latency = int(sys.argv[2])
spiir_branch = sys.argv[3]

with open(bg_output_file, 'w'):
    w.writelines("log written at: {}\n".format(get_current_time_string()))
    w.writelines("IFO: {}\n".format(IFO))
    w.writelines("latency: {}\n".format(latency))
    w.writelines("spiir_branch: {}\n\n".format(spiir_branch))
    w.writelines(
        "number of good templates: {}".format(number_of_bad_templates))
    w.writelines("number of bad templates: {}".format(number_of_bad_templates))

with open(bg_output_file, 'a') as a:
    record_machine_info_to_fh(a, False)
