# Copyright (C) 2020-2021 Alex Codoreanu
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""This function builds a new bank and then compares it to a previously built reference bank. The default number of
allowed 'bad' templates is currently set to 100 and can be reset by changing the value of the
number_of_allowed_bad_templates variable.

USAGE:
python build_and_compare_new_bank.py [PSD_NAME]
                                     [IFO]
                                     [NEGATIVE_LATENCY]
                                     [spiir_branch_name]


"""

from os import getenv
from sys import argv

from glue.ligolw import array
from glue.ligolw import ligolw
from glue.ligolw import param
from glue.ligolw import utils
from gstlal.spiirbank.cbc_template_iir import Bank
from lal.series import read_psd_xmldoc
from time import time
from unit_testing.base import base_path as unit_test_base_path
from unit_testing.base import compare_full_banks
from unit_testing.base import write_output_file_for_bank_comparison

array.use_in(ligolw.LIGOLWContentHandler)
param.use_in(ligolw.LIGOLWContentHandler)

number_of_allowed_bad_templates = 100

user_home_path = getenv("HOME")
bg_dependencies_path = "{}dependencies/".format(unit_test_base_path)
dependencies_path = "{}bank_generation".format(bg_dependencies_path)

try:
    reference_psd = argv[1]
    IFO = argv[2]
    negative_latency = int(str(argv[3]))
    spiir_branch = argv[4]

except Exception as local_exception:
    print(
        "Failed to pass command line arguments to variables with exception:\n{}\n"
        .format(local_exception))

    for arg_index in list(range(1, 5)):
        try:
            print("\nargument at index {}: {}\n".format(
                arg_index, argv[arg_index]))
        except Exception as sub_local_exception:
            print("failed to print argument at index {}\nwith exception: {}\n".
                  format(arg_index, sub_local_exception))

    print(
        "I'll be using default values for: reference_psd, ifo and negative latency"
    )

    reference_psd = '{}/gstlal_H1L1V1-REFERENCE_PSD-1186624818-687900.xml.gz'.format(
        dependencies_path)
    IFO = 'H1'
    negative_latency = 0

template_bank = '{}/original_split_banks/{}-GSTLAL_SPLIT_BANK_0003-0-0.xml.gz'.format(
    dependencies_path, IFO)
output_bank = "{}/{}-GSTLAL_SPLIT_BANK_0003-0-0-latency_{}_{}.xml.gz".format(
    user_home_path, IFO, negative_latency, spiir_branch)

print("\nPSD: {}\ntemplate: {}\noutput_bank: {}".format(
    reference_psd, template_bank, output_bank))

bank = Bank()
flow = 15.0
waveform_domain = 'FD'
padding = 1.3
autocorrelation_length = 351
sampleRate = 2048.0

epsilon_options = {
    "epsilon_start": 1.0,
    "nround_max": 25,
    "initial_overlap_min": 0.95,
    "b0_optimized_overlap_min": 0.97,
    "epsilon_factor": 1.2,
    "filters_max": 350
}

optimizer_options = {
    "verbose": True,
    "passes": 16,
    "indv": True,
    "hessian": True
}

approximant = 'SEOBNRv4_ROM'
verbose_bool = True
snr_cut = 0.998
downsample_bool = True

print("reading ALLpsd")
ALLpsd = read_psd_xmldoc(
    utils.load_filename(reference_psd,
                        verbose=verbose_bool,
                        contenthandler=ligolw.LIGOLWContentHandler))

start_build_time = time()

print("building bank")
bank.build_from_tmpltbank(template_bank,
                          sampleRate=sampleRate,
                          negative_latency=negative_latency,
                          all_psd=ALLpsd,
                          verbose=verbose_bool,
                          padding=padding,
                          flower=flow,
                          snr_cut=snr_cut,
                          downsample=downsample_bool,
                          optimizer_options=optimizer_options,
                          waveform_domain=waveform_domain,
                          approximant=approximant,
                          autocorrelation_length=autocorrelation_length,
                          debug=True,
                          keep_track=False,
                          **epsilon_options)

bank.write_to_xml(output_bank)
del bank

end_build_time = time()

print("Started build at: {}".format(start_build_time))
print("Ended build at: {}\n".format(end_build_time))

print("Starting to compare templates ...")
reference_bank_path = "{}bank_generation/reference_output_banks".format(
    bg_dependencies_path)
reference_bank_name = "{}/{}-GSTLAL_SPLIT_BANK_0003-0-0-_latency_{}.xml.gz".format(
    reference_bank_path, IFO, negative_latency)

log_file_name = '{}/bg_comparison_IFO_{}_latency_{}_{}.csv'.format(
    user_home_path, IFO, negative_latency, spiir_branch)

compare_full_banks(reference_bank_name, output_bank, log_file_name)
write_output_file_for_bank_comparison(log_file_name,
                                      number_of_allowed_bad_templates, IFO,
                                      negative_latency, spiir_branch)
