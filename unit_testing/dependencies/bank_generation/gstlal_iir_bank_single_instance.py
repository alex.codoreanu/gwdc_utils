"""
A single instance of bank production.

USAGE:
python gstlal_iir_bank_single_instance.py [PSD_NAME]
                                          [NEGATIVE_LATENCY]
                                          [template_bank_name]
                                          [output_bank_name]
                                          [single template to be made]
                                              OPTIONAL:
                                              if not passed, all templates will be made

OPTIONAL:
    if you want to log to a remote sql server then turn remote_log_bool to True within the code
    The current default database engine is:
            eng = GwdcDb().get_engine()

    This can be updated in the future to point to any other database

"""

from os import environ
from os.path import isfile
from sys import argv

from glue.ligolw import array
from glue.ligolw import ligolw
from glue.ligolw import param
from pandas import DataFrame
from time import time

array.use_in(ligolw.LIGOLWContentHandler)
param.use_in(ligolw.LIGOLWContentHandler)
from glue.ligolw import utils
from lal.series import read_psd_xmldoc
from gstlal.spiirbank.cbc_template_iir import Bank
from bank_utils.base import bank_utils_path
from db_utils.base import get_current_time_string

# change remote_log_bool to True if you want to log to SQL server
remote_log_bool = False

# default variables which will be changed if remote_log_bool is True
eng = None
remote_log_table_name = False

try:
    reference_psd = argv[1]
    negative_latency = int(str(argv[2]))
    template_bank = argv[3]
    output = argv[4]
    try:
        template = int(argv[5])
    except Exception as local_exception:
        print("I'll make a full bank as you did not pass me a "
              "single argument that can be cast to an int type.")
        template = 'all'

    try:
        master_log_sql_table = argv[6]
    except Exception as local_exception:
        print("No remote SQL table was passed so I will not log this job")
        remote_log_bool = False

except Exception as local_exception:
    print("Failed to pass command line arguments to variables.\n")

    print("\n{}\n".format(argv[1]))
    print("\n{}\n".format(argv[2]))
    print("\n{}\n".format(argv[3]))
    #    print("\n{}\n".format(argv[4]))

    print("Exception is: {}\n".format(local_exception))
    print(
        "I'll be using default values for: reference_psd, template_bank and output"
    )
    dependencies_path = "{}/dependencies".format(bank_utils_path)
    reference_psd = '{}/gstlal_H1L1V1-REFERENCE_PSD-1186624818-687900.xml.gz'.format(
        dependencies_path)
    template_bank = '{}/H1-GSTLAL_SPLIT_BANK_0003-0-0.xml.gz'.format(
        dependencies_path)
    output = '{}/H1-GSTLAL_SPLIT_BANK_0003-0-0.xml.gz'.format(environ['HOME'])
    negative_latency = 0
    template = 'all'

print("\nPSD: {}\ntemplate: {}\noutput: {}".format(reference_psd,
                                                   template_bank, output))

bank = Bank()
flow = 15.0
waveform_domain = 'FD'
padding = 1.3
autocorrelation_length = 351
sampleRate = 2048.0

if remote_log_bool:
    from db_utils import GwdcDb

    eng = GwdcDb().get_engine()
    master_log = DataFrame()
    master_log.loc[0, 'PSD'] = reference_psd
    master_log.loc[0, 'template_bank'] = template_bank
    master_log.loc[0, 'output_bank'] = output
    master_log.loc[0, 'template'] = str(template)

    for ifo_id in ['H1-', 'L1-', 'V1-']:
        if ifo_id in template_bank:
            remote_log_table_name = '{}_neg_lat_{}_{}'.format(
                ifo_id.replace("-", ""), negative_latency,
                get_current_time_string())
            instrument = ifo_id[:2]
            print("IFO is: {}".format(instrument))
            print("Remote log is written to: {}".format(remote_log_table_name))
            master_log['IFO'] = instrument
            master_log['remote_log'] = remote_log_table_name

    master_log.loc[0, 'flow'] = flow
    master_log.loc[0, 'waveform_domain'] = waveform_domain
    master_log.loc[0, 'padding'] = padding
    master_log.loc[0, 'autocorrelation_length'] = autocorrelation_length
    master_log.loc[0, 'sampleRate'] = sampleRate
    master_log.loc[0, 'negative_latency'] = negative_latency

epsilon_options = {
    "epsilon_start": 1.0,
    "nround_max": 25,
    "initial_overlap_min": 0.95,
    "b0_optimized_overlap_min": 0.97,
    "epsilon_factor": 1.2,
    "filters_max": 350
}

optimizer_options = {
    "verbose": True,
    "passes": 16,
    "indv": True,
    "hessian": True
}

approximant = 'SEOBNRv4_ROM'
verbose_bool = True
snr_cut = 0.998
downsample_bool = True

print("reading ALLpsd")
ALLpsd = read_psd_xmldoc(
    utils.load_filename(reference_psd,
                        verbose=verbose_bool,
                        contenthandler=ligolw.LIGOLWContentHandler))

start_build_time = time()

bank = Bank()

if template != 'all':
    bank.build_from_tmpltbank(template_bank,
                              templates=[template],
                              sampleRate=sampleRate,
                              negative_latency=negative_latency,
                              all_psd=ALLpsd,
                              verbose=verbose_bool,
                              padding=padding,
                              flower=flow,
                              snr_cut=snr_cut,
                              downsample=downsample_bool,
                              optimizer_options=optimizer_options,
                              waveform_domain=waveform_domain,
                              approximant=approximant,
                              autocorrelation_length=autocorrelation_length,
                              debug=True,
                              keep_track=False,
                              remote_log=remote_log_bool,
                              remote_db_engine=eng,
                              remote_log_table_name=remote_log_table_name,
                              **epsilon_options)
else:
    bank.build_from_tmpltbank(template_bank,
                              sampleRate=sampleRate,
                              negative_latency=negative_latency,
                              all_psd=ALLpsd,
                              verbose=verbose_bool,
                              padding=padding,
                              flower=flow,
                              snr_cut=snr_cut,
                              downsample=downsample_bool,
                              optimizer_options=optimizer_options,
                              waveform_domain=waveform_domain,
                              approximant=approximant,
                              autocorrelation_length=autocorrelation_length,
                              debug=True,
                              keep_track=False,
                              remote_log=remote_log_bool,
                              remote_db_engine=eng,
                              remote_log_table_name=remote_log_table_name,
                              **epsilon_options)

bank.write_to_xml(output)

if remote_log_bool:
    master_log.loc[0, 'elapsed_time'] = time() - start_build_time
    master_log_file = "{}/master_log.csv".format(environ['HOME'])

    try:
        if isfile(master_log_file):
            master_log.to_csv(master_log_file,
                              mode='a',
                              header=False,
                              index=False)
        else:
            master_log.to_csv(master_log_file, index=False)
    except Exception as local_exception:
        print('Could not write master_log file to:{}'.format(master_log_file))

    if master_log_sql_table is not None:
        master_log.to_sql(master_log_sql_table,
                          con=eng,
                          chunksize=master_log.elapsed_time.size,
                          if_exists='append',
                          index=False)

    del eng
