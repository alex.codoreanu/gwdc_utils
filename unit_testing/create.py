# Copyright (C) 2020-2021 Alex Codoreanu
# Copyright (C) 2020-2021 Patrick Clearwater
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os
from glob import glob
from os import environ as get_bash_env_variable
from os import getcwd
from os import getenv
from os import remove
from os import system as run_bash
from os import uname
from os.path import abspath
from sys import exit
import shutil

from bank_utils.base import current_IFO_short_names
from bank_utils.base import current_latency_values
from pandas import read_csv
from postprocess_utils.create import backgroundstats_xml_to_csv

import base
from base import append_line_to_file
from base import available_IFOs
from base import get_current_time_string
from base import run_single_trigger_comparison

base_path = str(abspath(base.__file__)).replace('/base.pyc', '')
base_path = base_path.replace('/base.py', '')

available_events = ['gw170817', 's190426c', 's190521r', 's190707q', 's190728q']


def trigger_comparison(verbose=True,
                       dependencies_dir=None,
                       offline_IFOs=None,
                       event_id='all'):
    """This test runs a comparison on the output of a newly run offline trigger.

    It runs a new trigger and then compares the resulting single snr series for each detector with an already
    created reference file. For further details see JIRA ticket SPIIP-76.

    Details are logged in an output file whose name depends on the time at which the code was run.
    It logs:
        - location of unit_testing module
        - the machine on which the code ran
        - all PYTHONPATH sub-paths
        - possible reasons for failure

    USAGE:

    from unit_testing.create import trigger_comparison
    result = trigger_comparison(event_id='gw170817')

    @param verbose: bool to display status to device
    @param dependencies_dir: location of dependencies
    @param offline_IFOs: list of offline instruments.
                         Current available IFOs list is: ['Livingston', 'Hanford', 'Virgo']


    @return: return_bool: bool result of test
    """

    local_path = getcwd()
    local_machine = uname()[1]

    log_file = '{}/trigger_comparison__{}.log'.format(
        local_path, get_current_time_string())
    print('writing log file:\n{}\n'.format(log_file))

    with open(log_file, 'w') as w:
        w.writelines('Starting log for trigger comparison file ... \n')

    test_events = None
    if event_id == 'all':
        test_events = available_events
    else:
        if type(event_id) is str:
            if event_id not in available_events:
                line = 'Event id: {} is not in available_events\n'.format(
                    event_id)
                append_line_to_file(log_file, line, verbose)
            else:
                test_events = [event_id]
        if type(event_id) is list:
            common_events = list(
                set(event_id).intersection(set(available_events)))
            if len(common_events) < len(event_id):
                missed_events = list(set(event_id) - set(common_events))
                line = 'The following events were passed but are not available: {}\n'.format(
                    missed_events)
                append_line_to_file(log_file, line, verbose)
                test_events = common_events

    if test_events is not None:
        line = 'Will compare events: {}\n'.format(test_events)
        append_line_to_file(log_file, line, verbose)
    else:
        line = 'I did not understand the passed event_id: {}\n'.format(
            event_id)
        append_line_to_file(log_file, line, verbose)
        return False

    test_bool = True
    for local_event in test_events:
        local_test_result = run_single_trigger_comparison(
            local_event, local_path, local_machine, offline_IFOs, verbose,
            dependencies_dir)
        if local_test_result == False:
            test_bool = False

        line = 'Trigger comparison for event {} : {}\n'.format(
            local_event, local_test_result)
        append_line_to_file(log_file, line, verbose)

    print('Full event log file: {}'.format(log_file))
    return test_bool


def get_skymap_comparison_events(get_dict=False):
    """Return a list of all valid skymap comparison events.

    The get_dict parameter is for internal use only.
    """
    valid_events = {
        'gw170817.0': ['gw170817_H1_1187008882_448730469_3_816', '1187008882'],
        'gw170817.1': ['gw170817_L1_1187008882_445312500_3_806', '1187008882'],
        's190426c.0':
        ['s190426c_H1_1240318870_423339843_186_80', '1240318870'],
        's190521r.0':
        ['s190521r_H1_1242459857_469238281_415_854', '1242459857'],
        's190521r.1':
        ['s190521r_H1_1242459857_485839844_415_939', '1242459857'],
        's190521r.2':
        ['s190521r_L1_1242459857_463867187_415_854', '1242459857'],
        's190707q.0':
        ['s190707q_H1_1246527224_167968750_383_209', '1246527224'],
        's190707q.1':
        ['s190707q_L1_1246527224_168945312_383_209', '1246527224'],
        's190728q.0':
        ['s190728q_H1_1248331528_534667969_404_812', '1248331528'],
        's190728q.1':
        ['s190728q_L1_1248331528_529785156_404_444', '1248331528'],
    }
    if get_dict:
        return valid_events
    else:
        return valid_events.keys()


def skymap_comparison(verbose=True,
                      event='gw170817.1',
                      save_prob=None,
                      save_cohsnr=None):
    """Test to see if skymap conversion, from raw form to PNG via FITS, is
    working. A number of test cases are available; by default (and for
    backwards compatibility) GW170817 will be used.

    The comparison at the end is tricky because we need to compare images, but
    would prefer not to fail just because a slightly different font is
    installed. We use ImageMagick's 'compare' command, although this may need
    to be revisited in future

    USAGE
    from unit_testing.create import skymap_comparison, get_skymap_comparison_events
    res = [skymap_comparison(event=e) for e in get_skymap_comparison_events()]
    print("%d/%d tests passed" % (sum(res), len(res)))

    @param verbose: be very chatty when making things happen
    @param event: string identifying which event to test
    @param save_prob: string identifying where to save probability map image
                      (None if you don't want to save it)
    @param save_cohsnr: string identifying where to save cohsnr map image
                        (None if you don't want to save it)

    @return: return_bool: result of the test

    Available skymaps (i.e. valid choices for 'event'):
     * gw170817.{0,1}
     * s190426c.0
     * s190521r.{0..2}
     * s190707q.{0,1}
     * s190728q.{0,1}
    """

    valid_events = get_skymap_comparison_events(get_dict=True)

    if not event in valid_events:
        print("Uh-oh, {} doesn't seem to be a valid event".format(event))
        return

    local_root = os.path.dirname(os.path.abspath(__file__))

    logfile = '{}/skymap_comparison_{}_{}.log'.format(
        local_root, base.get_current_time_string(), event)
    try:
        logfile_fh = open(logfile, 'w')
    except:
        print("Uh-oh, something went wrong attempting to open logfile: {}".
              format(logfile))
        return

    base.record_machine_info_to_fh(logfile_fh, verbose)

    # Remove PNGs from old runs, if they exist
    for old_file in ['prob.png', 'cohsnr.png']:
        try:
            filename = '{}/fits_skymap/output/{}'.format(local_root, old_file)
            remove(filename)
            base.append_line_to_fh(logfile_fh, 'Removed {}'.format(filename),
                                   verbose)
        except:
            pass  # fine to eat this; most likely the file doesn't exist

    cmd = 'bash {}/run_skymap.sh {} {}'.format(local_root,
                                               valid_events[event][0],
                                               valid_events[event][1])
    base.append_line_to_fh(logfile_fh, 'Command: {}'.format(cmd), verbose)
    run_bash(cmd)

    save_files_to = {'prob.png': save_prob, 'cohsnr.png': save_cohsnr}

    for new_file in ['prob.png', 'cohsnr.png']:
        filename = '{}/fits_skymap/output/{}'.format(local_root, new_file)
        # Do the files exist?
        if os.path.isfile(filename):
            base.append_line_to_fh(
                logfile_fh, 'Pass - check {} was created'.format(filename),
                verbose)
        else:
            base.append_line_to_fh(
                logfile_fh, 'FAIL - check {} was created'.format(filename),
                verbose)
            print("Looks like {} wasn't created, test failed".format(filename))
            return False
        # If needed, copy the image files
        if save_files_to[new_file]:
            shutil.copy(filename, save_files_to[new_file])
        # Check the images are vaguely similar to reference
        if base.images_similar(
                filename, '{}/fits_skymap/reference/{}_{}'.format(
                    local_root, valid_events[event][0], new_file)):
            base.append_line_to_fh(
                logfile_fh, 'Pass - {} similar to reference'.format(filename),
                verbose)
        else:
            base.append_line_to_fh(
                logfile_fh,
                'FAIL - check {} looks similar to reference'.format(filename),
                verbose)
            print(
                "Looks like {} doesn't look similar to reference, test failed".
                format(filename))
            return False

    base.append_line_to_fh(logfile_fh, 'Result: PASS', verbose)
    return True


def bank_generation_comparison(ifo='H1', latency=0, spiir_branch='master'):
    """This function writes and submits a SLURM job which first creates/compares a new comparison bank with an existing
    reference bank. The job runs the unit_testing/dependencies/bank_generation/build_and_compare_new_bank.py file.

    The individual templates (rows) of this comparison bank are compared one by one with each respective individual
    templates from the corresponding reference bank. The result of each template comparison is logged. This log is then
    used to identify if the test passed or failed. ALL intermediary files are written to the user's HOME directory.

    The intermediary files are:
        - the SLURM submission file: bg_test_<ifo>_<latency>.sh
        - associated .out and .err files are both of format: bg_unit_test_IFO_<ifo>_latency_<latency>.out/err
        - comparison bank: <ifo>-GSTLAL_SPLIT_BANK_0003-0-0-latency_<latency>_<spiir_branch>.xml.gz
        - log of each template comparison: bg_comparison_IFO_<ifo>_latency_<latency>_<spiir_branch>.csv
        - pass/fail log: bg_comparison_IFO_<ifo>_latency_<latency>_<spiir_branch>_<PASS/FAIL>.txt

    USAGE:
        from unit_testing.create import bank_generation_comparison
        bank_generation_comparison(ifo='L1', latency=10, spiir_branch='dd8616ed')

    @param ifo: instrument identifier; current options are ['L1', 'V1', 'H1']
    @param latency: the latency of the bank; current options are [0, 10, 20, 30]
    @param spiir_branch: the spiir branch you wish to use for the bank generation regression test

    @return:
    """

    if ifo not in current_IFO_short_names:
        line = "ERROR: you passed me ifo = {}.\nCurrent available IFOS list is: {}.\n".format(
            ifo, available_IFOs)
        print(line)
        exit(1)

    if latency not in current_latency_values:
        line = "ERROR: you passed me latency = {}.\ncurrent_latency_values list is: {}.\n".format(
            latency, current_latency_values)
        print(line)
        exit(1)

    shell_name = base.write_submission_script(ifo, latency, spiir_branch)
    run_bash("sbatch {}".format(shell_name))

    print("submitted {}\n".format(shell_name))


# noinspection DuplicatedCode,SpellCheckingInspection
def backgroundstats_comparison(verbose=True,
                               dependencies_dir=None,
                               infile=None,
                               offline_IFOs=None):
    """This test runs a comparison on the output of a newly run background stats.

    It runs a background PDF estimation script. It reads the output which is a background stats
    file and then compares the resulting individual and the network PDF
    tables with an already created reference file.

    Details are logged in an output file whose name depends on the time at which the code was run.
    It logs:
        - location of unit_testing module
        - the machine on which the code ran
        - all PYTHONPATH sub-paths
        - possible reasons for failure

    USAGE:
    from unit_testing.create import backgroundstats_comparison
    result = backgroundstats_comparison()

    @param verbose: bool to display status to device
    @param dependencies_dir: location of dependencies
    @param infile: the stats file from pipeline run
    @param offline_IFOs: list of offline instruments.
                         Current available IFOs list is: ['Livingston', 'Hanford', 'Virgo']


    @return: return_bool: bool result of test
    """

    # get the offline_detector_ids which will be needed to remove offline detector variables
    # from relevant setup_file parameters
    if offline_IFOs is not None:
        for offline_IFO in offline_IFOs:
            if offline_IFO not in available_IFOs:
                print('Instrument {} is not in list of available_IFOs.'.format(
                    offline_IFO))
                print('Current available IFOs list is: {}'.format(
                    available_IFOs))
                exit(1)
        reference_keys = read_csv(
            '{}/dependencies/stats__keys.csv'.format(base_path))
        offline_detector_ids = reference_keys.loc[reference_keys['IFO'].isin(
            offline_IFOs)].IFO_id.unique().tolist()

    local_path = getcwd()
    local_machine = uname()[1]
    if verbose:
        print "set log file"

    local_log_file = '{}/backgroundstats_comparison_{}.log'.format(
        local_path, base.get_current_time_string())

    try:
        with open(local_log_file, 'w') as w:
            w.writelines('Starting log for background stats comparison ... \n')
    except OSError:
        owner_header = 'You do not have write privileges in:\n{}'.format(
            local_path)
        base.append_line_to_file(local_log_file, owner_header, verbose)
        user = base.get_user(local_path)
        group = base.get_group(local_path)
        owner_line = 'The folder is owned by:' \
                     '\nuser: {}\ngroup: {}'.format(user, group)
        base.append_line_to_file(local_log_file, owner_line, verbose)

        print "Log written to:\n{}".format(local_log_file)
        return False

    print '\nWriting test output to file:\n{}\n'.format(local_log_file)

    if offline_IFOs is not None:
        if verbose:
            output_line = '{} detectors are offline.\n'.format(offline_IFOs)
            base.append_line_to_file(local_log_file, output_line, verbose)

    # identify code base and machine on which it's running
    line = "Testing code is located at: {}\n".format(base_path)
    base.append_line_to_file(local_log_file, line, verbose)

    line = "Machine that ran the code is: {}\n".format(local_machine)
    base.append_line_to_file(local_log_file, line, verbose)

    python_path_header = 'Your PYTHONPATH points to the following directories:\n'
    base.append_line_to_file(local_log_file, python_path_header, verbose)

    if infile is None:
        stats_file = glob('*bankstats_out*')
    else:
        stats_file = [infile]

    if len(stats_file) != 1:
        if len(stats_file) == 0:
            status_line = "No background input file, please check {}.\n".format(
                stats_file)
            base.append_line_to_file(local_log_file, status_line, verbose)
        else:
            status_line = "Mulitple marginalization files were found in *bankstats_out*: {}\n".format(
                stats_file)
            base.append_line_to_file(local_log_file, status_line, verbose)

        print "Log written to:\n{}".format(local_log_file)
        return False

    for sub_path in str.split(str(get_bash_env_variable['PYTHONPATH']), ':'):
        base.append_line_to_file(local_log_file, '{}\n'.format(sub_path),
                                 verbose)

    print "\n\n Starting to run the background marginalization script \n\n"
    margifile = "banktmp.xml.gz"

    run_line = 'bash {}/run_calcfap.sh {} {}'.format(base_path, stats_file[0],
                                                     margifile)

    base.append_line_to_file(local_log_file,
                             '\n run_line = {}'.format(run_line), verbose)
    run_bash(run_line)

    if not os.path.isfile(margifile):
        status_line = "Background marginalization file was not created: {}.\n".format(
            margifile)
        base.append_line_to_file(local_log_file, status_line, verbose)
        print "Log written to:\n{}".format(local_log_file)
        return False
    else:
        print "Marginalization ran successfully.\n"

    print "Creating .csv from stats file ... "

    output_file_id = margifile.replace('xml.gz', 'csv')

    if offline_IFOs is None:
        backgroundstats_xml_to_csv(margifile,
                                   setup_file_name='backgroundstats_setup',
                                   output_file_path=local_path,
                                   output_file_name=output_file_id,
                                   verbose=verbose)
    else:
        backgroundstats_xml_to_csv(margifile,
                                   setup_file_name='backgroundstats_setup',
                                   output_file_path=local_path,
                                   output_file_name=output_file_id,
                                   verbose=verbose,
                                   offline_detector_ids=offline_detector_ids)

    stats_csv_name = '{}/{}'.format(local_path, output_file_id)
    baseline_file = '{}/dependencies/stats__summary.csv'.format(base_path)

    if offline_IFOs is None:
        test_bool = base.compare_background_stats(stats_csv_name,
                                                  reference_file=baseline_file)
    else:
        test_bool = base.compare_background_stats(
            stats_csv_name,
            reference_file=baseline_file,
            offline_detector_ids=offline_detector_ids)

    if test_bool:
        status_line = "\nComparison test: PASSED\n"
    else:
        status_line = "\nComparison test: FAILED\nstats_file:{}\nbaseline_file:{}\n".format(
            stats_csv_name, baseline_file)

    base.append_line_to_file(local_log_file, status_line, verbose)
    print "Log written to:\n{}".format(local_log_file)

    return test_bool
