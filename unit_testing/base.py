# Copyright (C) 2020-2021 Alex Codoreanu
# Copyright (C) 2020-2021 Patrick Clearwater
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os
import pwd
import subprocess
from sys import exit
from glob import glob
from os import remove
from os import system as run_bash

import datetime
import grp
from os import environ as get_bash_env_variable
from bank_utils.base import \
    compare_template_parameters, \
    get_bank_object, \
    get_common_rate, \
    get_single_template_parameters_from_bank_object
from plot_utils.create import scatter_plot
from pandas import read_csv
from plot_utils import base as base_temp
from postprocess_utils.create import combined_dataframe_from_xml_files

STATS_LEN = 300
available_IFOs = ['Livingston', 'Hanford', 'Virgo']

# get base_path
base_path = os.path.abspath(base_temp.__file__)
del base_temp
base_path = base_path[:base_path.find('plot_utils')]
base_path = '{}unit_testing/'.format(base_path)


def append_line_to_file(local_log_file, line, verbose):
    """Append line to existing file.

    @param local_log_file: file to append to
    @param line: line to append
    @param verbose: bool to print line to device
    @return:
    """
    if verbose:
        print(line)
    with open(local_log_file, 'a') as a:
        a.writelines(line)


def append_line_to_fh(logfile_fh, line, verbose):
    """Append a line to the given file handle, optionally printing it.

    @param logfile_fh: file handle
    @param line: line to print
    @param verbose: whether to print to stdout as well
    """
    if verbose:
        print(line)
    logfile_fh.write(line + '\n')


def record_machine_info_to_fh(logfile_fh, verbose):
    """Record common information to the given logfile file handle.

    This common information is e.g. the machine being run on, location of test
    code, environment variables, and other things that may affect test
    execution

    @param logfile_fh: file handle
    @param verbose: whether to print to stdout as well
    """

    append_line_to_fh(logfile_fh, 'Machine name: '.format(os.uname()[1]),
                      verbose)
    for var in ['PYTHONPATH', 'PATH', 'LD_LIBRARY_PATH']:
        append_line_to_fh(logfile_fh, var + ':', verbose)
        for p in str.split(str(os.environ[var]), ':'):
            append_line_to_fh(logfile_fh, '  {}'.format(p), verbose)


def images_similar(f1, f2, threshold=5):
    """Use the Imagemagick 'compare' utility to check if two images are vaguely
    similar. The threshold is based on IM's 'AE' (absolute error) metric and
    for now is a guess.

    @param f1: name of image to compare
    @param f2: name of image to compare
    @param threshold: reference threshold
    """

    # For future Python 3 implementation:
    #       result = subprocess.subprocess.run(
    #       ['compare', '-verbose', '-metric', 'AE', f1, f2, '/dev/null'],
    #       capture_output=True, text=True)
    #       for l in result.stderr.splitlines():

    try:
        compare_output = subprocess.check_output(
            ['compare', '-verbose', '-metric', 'AE', f1, f2, '/dev/null'],
            universal_newlines=True,
            stderr=subprocess.STDOUT)
    except:
        print("Uh-oh, subrocess.check_output threw", f1, f2)
        return False

    for local_line_el in compare_output.splitlines():
        print local_line_el
        if local_line_el.startswith('    all: '):
            difference = int(local_line_el.split(': ')[1])
            if difference > threshold:
                return False
            else:
                return True

    # We never saw what we needed in the output, yikes
    print('Uh-oh, internal error comparing {} with {}'.format(f1, f2))
    return False


def get_uid(temp_path):
    """Get uid of folder: temp_path.

    @param temp_path: target directory
    @return: uid
    """
    stat_info = os.stat(temp_path)
    return stat_info.st_uid


def get_gid(temp_path):
    """Get gid of folder: temp_path.

    @param temp_path: target directory
    @return: gid
    """
    stat_info = os.stat(temp_path)
    return stat_info.st_gid


def get_user(temp_path):
    """Get user of folder: temp_path.

    @param temp_path: target directory
    @return: user
    """
    return pwd.getpwuid(get_uid(temp_path))[0]


def get_group(temp_path):
    """Get group of folder: temp_path.

    @param temp_path: target directory
    @return: group
    """

    return grp.getgrgid(get_gid(temp_path))[0]


def get_current_time_string():
    """This function returns a time string where the datetime output
    has underscores instead of local_el in ['-', ' ', ':', '.'].

    @return:  current_time_string

    """

    # get time_stamp string
    current_time = str(datetime.datetime.now())
    for local_el in ['-', ' ', ':', '.']:
        current_time = current_time.replace(local_el, '_')

    return str(current_time)


def compare_background_stats(comparison_file,
                             reference_file=None,
                             reference_keys=None,
                             threshold_value=None,
                             offline_detector_ids=None):
    """

    USAGE:
    from unit_testing.base import compare_background_stats

    # compare file to itself
    comparison_file = 'stats__summary.csv'

    if compare_background_stats(comparison_file):
        'compare background stats files test: PASS'
    else:
        'compare background stats files test: FAIL'


    @param offline_detector_ids:
    @return:
    @param comparison_file: new file to be compared with reference file
    @param reference_file: reference file to be used for comparison. Description of default file
                           is available in associated JIRA ticket SPIIP-76.
    @param reference_keys:
    @param threshold_value:
    @return: bool

    """

    return_bool = True

    if threshold_value is None:
        threshold_value = 0.00001

    # read in the reference keys from file
    if reference_keys is None:
        try:
            reference_key_file = '{}/dependencies/stats__keys.csv'.format(
                base_path)
            reference_keys = read_csv(
                reference_key_file).reference_keys.tolist()
            for reference_key in reference_keys:
                if offline_detector_ids is not None:
                    for offline_detector_id in offline_detector_ids:
                        if offline_detector_id in reference_key[-4:]:
                            reference_keys.remove(reference_key)

        except Exception as local_exception:
            print('Failed with Exception: {}'.format(local_exception))
            exit(1)

    if reference_file is None:
        reference_file = 'dependencies/stats__summary.csv'

    # FIXME: randomly picking rows to compare
    ref_dict = {}
    cmp_dict = {}
    with open(reference_file, 'r') as f_ref:
        ref_lines = f_ref.readlines()
        iline = 0
        while (iline < len(ref_lines)):
            ref_line = ref_lines[iline]
            if ref_line.rstrip("\n") in reference_keys:
                ref_dict[ref_line.rstrip("\n")] = ref_lines[iline + 1:iline +
                                                            STATS_LEN + 1]
                iline += STATS_LEN + 1
            else:
                iline += 1

    with open(comparison_file, 'r') as f_cmp:
        cmp_lines = f_cmp.readlines()
        iline = 0
        while (iline < len(cmp_lines)):
            cmp_line = cmp_lines[iline]
            if cmp_line.rstrip("\n") in reference_keys:
                cmp_dict[cmp_line.rstrip("\n")] = cmp_lines[iline + 1:iline +
                                                            301]
                iline += STATS_LEN + 1
            else:
                iline += 1

    if len(cmp_dict.keys()) < 1:
        print "No {} table the marginalization file to compare\n".format(
            reference_keys)
        return False

    for key in cmp_dict.keys():
        if cmp_dict[key] != ref_dict[key]:
            return False

    return True


def compare_zerolag_trigger_files(comparison_file,
                                  reference_file=None,
                                  reference_keys=None,
                                  threshold_value=None,
                                  offline_detector_ids=None,
                                  plot_bool=True,
                                  event_id=None):
    """

    USAGE:
    from unit_testing.base import compare_zerolag_trigger_files

    # compare file to itself
    comparison_file = 'postcoh__summary.csv'

    if compare_zerolag_trigger_files(comparison_file):
        'compare zerolag files test: PASS'
    else:
        'compare zerolag files test: FAIL'


    @param offline_detector_ids:
    @return:
    @param comparison_file: new file to be compared with reference file
    @param reference_file: reference file to be used for comparison. Description of default file
                           is available in associated JIRA ticket SPIIP-76.
    @param reference_keys:
    @param threshold_value:
    @return: bool

    """

    return_bool = True

    if threshold_value is None:
        threshold_value = 0.00001

    # read in the reference keys from file
    if reference_keys is None:
        try:
            reference_key_file = '{}/dependencies/reference__keys.csv'.format(
                base_path)
            IFOS = read_csv(reference_key_file).IFO.tolist()
            reference_keys = read_csv(
                reference_key_file).reference_keys.tolist()
            for reference_key in reference_keys:
                if offline_detector_ids is not None:
                    for offline_detector_id in offline_detector_ids:
                        if offline_detector_id in reference_key[-4:]:
                            reference_keys.remove(reference_key)

        except Exception as local_exception:
            print('Failed with Exception: {}'.format(local_exception))
            exit(1)

    if reference_file is None:
        reference_file = 'dependencies/reference__summary_{}.csv'.format(
            event_id)

    try:
        reference_data = read_csv(reference_file)
        comparison_data = read_csv(comparison_file)
    except (OSError, IOError):
        print('Could not read in: \n{}\n{}'.format(reference_file,
                                                   comparison_file))
        return_bool = False
        return return_bool

    # check that the keys are identical between the two files
    diff_keys = set(reference_data.keys()) - set(comparison_data.keys())
    if len(diff_keys) > 0:
        print 'Keys are not the same between the reference and comparison files'
        check_keys = diff_keys.intersection(set(reference_keys))
        print("The following extra keys have been found: {}".format(diff_keys))

        if len(check_keys) > 0:
            print 'The following required keys are missing:{}'.format(
                list(check_keys))
            return_bool = False

    # find the common end_time values between the two csv files
    # and extract those subsets
    common_times = list(
        set(reference_data.end_time).intersection(set(
            comparison_data.end_time)))

    if len(common_times) == 0:
        print('There are no common end_time rows between '
              'the reference and comparison dataframes.')

        return_bool = False

    else:
        reference_data.set_index([reference_data.end_time.tolist()],
                                 inplace=True)
        comparison_data.set_index([comparison_data.end_time.tolist()],
                                  inplace=True)

        for key in reference_keys:
            for common_time in common_times:
                if len([reference_data.loc[common_time][key]]) == len(
                    [comparison_data.loc[common_time][key]]) == 1:
                    test_data = reference_data.loc[common_time][key] - \
                                comparison_data.loc[common_time][key]
                    if test_data.max() > threshold_value:
                        print(
                            'Difference between comparison array [{}]: {}\n is greater than '
                            'the current threshold value: {}'.format(
                                key, test_data.max(), threshold_value))
                        return_bool = False

        if plot_bool:
            x_data = []
            y_data = []
            for key in reference_keys:
                x_data = x_data.append(
                    reference_data.loc[common_time]['end_time'])
                y_data = y_data.append(comparison_data.loc[common_time][key])

            x_data = x_data[:3]
            snr_variables = reference_keys[:3]
            chi2_variables = reference_keys[3:]

            label_variables = IFOS[:3]

            # make SNR variables plot
            plot_name = 'trigger_comparison_snr_{}'.format(event_id)
            plot_name = '{}.png'.format(plot_name)
            scatter_plot(x_data,
                         snr_variables,
                         plot_name,
                         data_labels=label_variables,
                         xlabel='gps time',
                         ylabel='SNR',
                         background=False,
                         custom_style=dict(figsize=(17, 7), dpi=100))

            # make chi2 variables plot
            plot_name = 'trigger_comparison_chi2_{}'.format(event_id)
            plot_name = '{}.png'.format(plot_name)
            scatter_plot(x_data,
                         chi2_variables,
                         plot_name,
                         data_labels=label_variables,
                         xlabel='gps time',
                         ylabel='$\chi^2$',
                         background=False,
                         custom_style=dict(figsize=(17, 7), dpi=100))

    return return_bool


def write_submission_script(ifo, latency, spiir_branch='master'):
    """This function writes a SLURM submission script for the bank generation regression test:
    unit_testing/create.bank_generation_comparison()

    @param ifo: instrument string identifier
    @param latency: integer latency value
    @param spiir_branch: string identifier of ALREADY built branch to be tested

    @return:
    """

    user_home_path = os.getenv("HOME")
    bg_dependencies_path = "{}dependencies/bank_generation/".format(base_path)

    shell_name = "{}/bg_test_{}_{}.sh".format(user_home_path, ifo, latency)
    print("writing shell script: {}".format(shell_name))

    with open(shell_name, 'w') as w:
        w.writelines("#!/bin/bash\n")
        w.writelines("#SBATCH --ntasks=1\n")
        w.writelines("#SBATCH --time=102:00:00\n")
        w.writelines("#SBATCH --mem-per-cpu=4g\n")
        w.writelines("#SBATCH --gres=gpu:1\n")
        w.writelines("#SBATCH --job-name={}_{}\n".format(ifo, latency))
        w.writelines("#SBATCH --output="
                     "bg_unit_test_IFO_{}_latency_{}.out\n".format(
                         ifo, latency))
        w.writelines("#SBATCH --error="
                     "bg_unit_test_IFO_{}_latency_{}.err\n\n".format(
                         ifo, latency))

        w.writelines(
            ". /fred/oz016/gwdc_spiir_pipeline_codebase/scripts_n_things/build/bash_helper_functions.sh\n"
        )
        w.writelines("\n\nload_spiir {}\n\n".format(spiir_branch))

        w.writelines(
            ". /fred/oz016/gwdc_spiir_pipeline_codebase/gwdc_utils/add_gwdc_utils.sh \n"
        )
        w.writelines("\n\nadd_gwdc_utils\n\n")

        w.writelines("cd {}\n\n".format(bg_dependencies_path))

        w.writelines(
            "PSD={}"
            "gstlal_H1L1V1-REFERENCE_PSD-1186624818-687900.xml.gz\n".format(
                bg_dependencies_path))

        w.writelines(
            "output_bank=$HOME/"
            "{}-GSTLAL_SPLIT_BANK_0003-0-0-latency_{}_{}.xml.gz\n\n\n".format(
                ifo, latency, spiir_branch))

        w.writelines('echo "using PSD: ${PSD}"\n')
        w.writelines('echo "using spiir_branch: {}"\n\n'.format(spiir_branch))

        w.writelines(
            "python build_and_compare_new_bank.py {} {} {} {} \n\n".format(
                '${PSD}', ifo, latency, spiir_branch))

        return shell_name


def compare_bank_template(reference_bank,
                          comparison_bank,
                          sampling_rate,
                          template_number,
                          pct_threshold=None):
    """This function compares a template row between two banks. It extracts the ['a1', 'b0', 'd'] parameters and then
    calls compare_template_parameters() which returns a bool.

    @param reference_bank:
    @param comparison_bank:
    @param template_number:
    @param pct_threshold:
    @return: check_bool
    """

    reference_names = ['a1', 'b0', 'd']

    reference_a1, \
    reference_b0, \
    reference_d = get_single_template_parameters_from_bank_object(reference_bank,
                                                                  rate=sampling_rate,
                                                                  template_id=template_number)

    comparison_a1, \
    comparison_b0, \
    comparison_d = get_single_template_parameters_from_bank_object(comparison_bank,
                                                                   rate=sampling_rate,
                                                                   template_id=template_number)

    reference_items = [reference_a1, reference_b0, reference_d]
    comparison_items = [comparison_a1, comparison_b0, comparison_d]

    check_bool = compare_template_parameters(reference_items=reference_items,
                                             comparison_items=comparison_items,
                                             reference_names=reference_names,
                                             pct_theshold=pct_threshold)

    return check_bool


def compare_full_banks(reference_bank_name,
                       comparison_bank_name,
                       log_file_name,
                       verbose=True):
    """This function is a wrapper for unit_testing.base.compare_bank_template() to compare two full banks and log the
    result of each comparison.

    @param reference_bank_name:
    @param comparison_bank_name:
    @param log_file_name:
    @param verbose:
    @return:
    """

    print("Logging template comparison results to: {}".format(log_file_name))

    with open(log_file_name, 'w') as w:
        w.writelines('rate,template,test_result\n')

    reference_bank = get_bank_object(reference_bank_name)
    comparison_bank = get_bank_object(comparison_bank_name)

    print("\nRead in:\n "
          "reference_bank:{}\n "
          "comparison_bank:{}\n".format(reference_bank_name,
                                        comparison_bank_name))

    templates = list(range(len(comparison_bank.sngl_inspiral_table)))

    print("\nStarting to compare {} templates.\n".format(len(templates)))

    common_rates = get_common_rate(reference_bank, comparison_bank)

    for rate in common_rates:
        for template in templates:
            check_bool = compare_bank_template(reference_bank, comparison_bank,
                                               rate, template)

            with open(log_file_name, 'a') as a:
                output_line = "{},{},{}\n".format(rate, template, check_bool)
                a.writelines(output_line)
                if verbose:
                    print(output_line)

            if verbose:
                print("template: {} -- rate: {}".format(template, rate))

        print("Finished comparing {} templates for sampling frequency: {}.\n".
              format(len(templates), rate))


def write_output_file_for_bank_comparison(log_file_name,
                                          number_of_allowed_bad_templates, IFO,
                                          latency, spiir_branch):
    """This function reads in a log file written by unit_testing.base.compare_full_banks() and writes the final output
    file for the unit_testing.create.bank_generation_comparison() function.

    @param log_file_name:
    @param number_of_allowed_bad_templates:
    @param IFO:
    @param latency:
    @param spiir_branch:
    @return:
    """

    log = read_csv(log_file_name)
    print("Read in {}".format(log_file_name))

    test_bool = True
    number_of_bad_templates = len(
        log.loc[log.test_result == False].index.tolist())
    number_of_good_templates = len(
        log.loc[log.test_result != False].index.tolist())
    if number_of_bad_templates > number_of_allowed_bad_templates:
        test_bool = False

    if test_bool:
        print("BANK GENERATION REGRESSION TEST PASSED\n")
    else:
        print("BANK GENERATION REGRESSION TEST FAILED\n")

    bg_output_file = log_file_name.replace('.csv', '')

    if test_bool:
        bg_output_file = "{}_PASS.txt".format(bg_output_file)
    else:
        bg_output_file = "{}_FAIL.txt".format(bg_output_file)

    print("writing final log file: {}".format(bg_output_file))

    with open(bg_output_file, 'w') as w:
        w.writelines("log written at: {}\n".format(get_current_time_string()))
        w.writelines("IFO: {}\n".format(IFO))
        w.writelines("latency: {}\n".format(latency))
        w.writelines("spiir_branch: {}\n\n".format(spiir_branch))
        w.writelines(
            "number of good templates: {}\n".format(number_of_good_templates))
        w.writelines(
            "number of bad templates: {}\n\n".format(number_of_bad_templates))

    with open(bg_output_file, 'a') as a:
        record_machine_info_to_fh(a, False)


def get_reference_bank_name(IFO='L1', negative_latency=0):
    bg_dependencies_path = "{}dependencies/".format(base_path)
    reference_bank_path = "{}bank_generation/reference_output_banks".format(
        bg_dependencies_path)
    reference_bank_name = "{}/{}-GSTLAL_SPLIT_BANK_0003-0-0-_latency_{}.xml.gz".format(
        reference_bank_path, IFO, negative_latency)

    return reference_bank_name


def run_single_trigger_comparison(event_id, local_path, local_machine,
                                  offline_IFOs, verbose, dependencies_dir):

    # get the offline_detector_ids which will be needed to remove offline detector variables
    # from relevant setup_file parameters
    reference_keys = read_csv(
        '{}/dependencies/reference__keys.csv'.format(base_path))

    if offline_IFOs is not None:
        for offline_IFO in offline_IFOs:
            if offline_IFO not in available_IFOs:
                print('Instrument {} is not in list of available_IFOs.'.format(
                    offline_IFO))
                print('Current available IFOs list is: {}'.format(
                    available_IFOs))
                exit(1)

        offline_detector_ids = reference_keys.loc[reference_keys['IFO'].isin(
            offline_IFOs)].IFO_id.unique().tolist()

    local_log_file = '{}/trigger_comparison_event_{}_{}.log'.format(
        local_path, event_id, get_current_time_string())

    try:
        with open(local_log_file, 'w') as w:
            w.writelines(
                'Starting log for {} comparison ... \n'.format(event_id))
    except OSError:
        owner_header = 'You do not have write privileges in:\n{}'.format(
            local_path)
        append_line_to_file(local_log_file, owner_header, verbose)
        user = get_user(local_path)
        group = get_group(local_path)
        owner_line = 'The folder is owned by:' \
                     '\nuser: {}\ngroup: {}'.format(user, group)
        append_line_to_file(local_log_file, owner_line, verbose)

        print "Log written to:\n{}".format(local_log_file)
        return False

    print '\nWriting test output to file:\n{}\n'.format(local_log_file)

    if offline_IFOs is not None:
        if verbose:
            output_line = '{} detectors are offline.\n'.format(offline_IFOs)
            append_line_to_file(local_log_file, output_line, verbose)

    # identify code base and machine on which it's running
    line = "Testing code is located at: {}\n".format(base_path)
    append_line_to_file(local_log_file, line, verbose)

    line = "Machine that ran the code is: {}\n".format(local_machine)
    append_line_to_file(local_log_file, line, verbose)

    python_path_header = 'Your PYTHONPATH points to the following directories:\n'
    append_line_to_file(local_log_file, python_path_header, verbose)

    for sub_path in str.split(str(get_bash_env_variable['PYTHONPATH']), ':'):
        append_line_to_file(local_log_file, '{}\n'.format(sub_path), verbose)

    zerolag_directory = '{}/000'.format(local_path)

    # remove previous zerolag files
    previous_zerolag_files = glob('{}/*zerolag*'.format(zerolag_directory))

    if len(previous_zerolag_files) > 0:
        for output_zerolag_file in previous_zerolag_files:
            remove(output_zerolag_file)
            print '\nRemoved {}.'.format(output_zerolag_file)

    print "\n\n Starting to run pipeline \n\n"
    run_pipeline_line = 'bash {}run_{}_offline.sh'.format(base_path, event_id)
    if dependencies_dir is not None:
        run_pipeline_line = '{} {}'.format(run_pipeline_line, dependencies_dir)

    append_line_to_file(local_log_file,
                        '\n run_pipeline_line = {}'.format(run_pipeline_line),
                        verbose)

    run_bash(run_pipeline_line)

    zerolag_files = glob('{}/*zerolag*'.format(zerolag_directory))
    if len(zerolag_files) != 1:
        if len(zerolag_files) == 0:
            status_line = "Zerolag file was not created."
            append_line_to_file(local_log_file, status_line, verbose)
        else:
            status_line = "Multiple zerolag files were found in dir: \n{}\n.".format(
                zerolag_directory)
            append_line_to_file(local_log_file, status_line, verbose)

        print "Log written to:\n{}".format(local_log_file)
        return False

    else:
        print "Pipeline ran successfully.\n"

    print "Creating .csv from zerolag file ... "

    output_file_id = zerolag_files[0][zerolag_files[0].
                                      find('zerolag_'):].replace(
                                          'xml.gz', 'csv')

    if offline_IFOs is None:
        combined_dataframe_from_xml_files(zerolag_directory,
                                          "*zerolag*gz",
                                          setup_file_name='postcoh_setup',
                                          output_file_path=local_path,
                                          output_file_name=output_file_id,
                                          verbose=verbose)
    else:
        combined_dataframe_from_xml_files(
            zerolag_directory,
            "*zerolag*gz",
            setup_file_name='postcoh_setup',
            output_file_path=local_path,
            output_file_name=output_file_id,
            verbose=verbose,
            offline_detector_ids=offline_detector_ids)

    zerolag_csv_name = '{}/{}'.format(local_path, output_file_id)
    baseline_file = '{}/dependencies/reference__summary_{}.csv'.format(
        base_path, event_id)

    if offline_IFOs is None:
        test_bool = compare_zerolag_trigger_files(zerolag_csv_name,
                                                  reference_file=baseline_file)
    else:
        test_bool = compare_zerolag_trigger_files(
            zerolag_csv_name,
            reference_file=baseline_file,
            offline_detector_ids=offline_detector_ids)

    if test_bool:
        status_line = "\nComparison test: PASSED\n"
    else:
        status_line = "\nComparison test: FAILED\nzerolag_file:{}\nbaseline_file:{}\n".format(
            zerolag_csv_name, baseline_file)

    append_line_to_file(local_log_file, status_line, verbose)
    print "Log written to:\n{}".format(local_log_file)

    return test_bool
