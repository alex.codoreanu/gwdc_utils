#!/usr/bin/env python
# Copyright (C) 2020-2021 Alex Codoreanu, Patrick Clearwater
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
General Notes:
    This is a testing procedure to identify if your SPIIR working environment can connect to
    the SQL server available on OzStar.

    Currently, this test is run interactively inside a python REPL on OzStar and does not return a bool.

Usage:
	- once you've ssh'ed into OzStar you can then:
		- open a python REPL and simply run the following import:
         	import testConnection
        - run  the file inside it's directory (in the directory of the file):
            python testConnection.py
    - can also be used as a test of your remote interpreter from your local machine by
        just running it in PyCharm


"""
import sys

print("** Testing GWDC database connection **")

try:
    import db_utils
    print("  -- Successfully imported db_utils")

except Exception as e:
    print("\nWhen trying to load db_utils, got this exception:\n")
    print(e)
    print("Maybe you need to make sure 'dependencies' is in your PYTHONPATH?")
    sys.exit(1)

try:
    db = db_utils.GwdcDb()

except Exception as e:
    print("\nCouldn't connect to the database, because:")
    print(e)
    sys.exit(1)

with db.get_connection() as conn:
    try:
        print("  -- Available base tables:")
        sql_command = "SELECT * FROM information_schema.tables WHERE table_type='base table'"
        tables = conn.execute(sql_command)
        for r in tables:
            print("      * " + r['TABLE_NAME'])
    except Exception as e:
        print(
            "Failed to execute SQL command using the connection provided/\n{}".
            format(e))

print("\n** Successfully connected to the database! **")
