from os import mkdir
from sys import exit
from glob import glob
from shutil import rmtree as rmdir
from os.path import isfile
from os.path import isdir

# latex document output will be
# folder = section
# each .py = subsection
# each def () = subsubsection

# CHANGE THE FOLLOWING VARIABLES TO POINT TO/EXTRACT THE RELEVANT CODEBASE FOR EACH CASE
code_directory_path_var = None
directory_flags_var = ['*_utils', '*_testing']
root_directory_var = 'GWDC_UTILS_manual/code_documentation'
ignore_py_string_var = ['_dev.py', '.txt']

latex_root_directory = root_directory_var[root_directory_var.find('/') + 1:]


def main(code_directory_path,
         directory_flags,
         root_directory,
         ignore_py_string,
         verbose=True):
    """This function extracts each function documentation from a given project directory
    identified by the code_directory_path variable.

    The function looks into the project directory and finds all directories identified by elements
    in the directory_flags list.

    It then enters each identified sub-directory and finds all files within. It ignores all files
    which contain an element from the ignore_py_string list.

    :param code_directory_path: string denoting the location of the codebase you wish to document
    :param directory_flags: list containing string identifiers for relevant directories to be parsed
    :param root_directory: where to output the .tex and extracted documentation
    :param ignore_py_string: list containing string identifiers for files to be ignored
    :param verbose: boolean which turns on various progress messages.
                    DEFAULT value is True.
    :return:
    """

    if isdir(root_directory):
        try:
            rmdir(root_directory)

        except (IOError, OSError):
            print 'Could not clean previous build\n' \
                  'Make sure you have write permissions in this directory'
            exit(1)

    try:
        mkdir(root_directory)
        if verbose:
            print 'Created the base directory:\n{}'.format(root_directory)

    except (IOError, OSError):
        print 'Could not create required directory\n' \
              'Make sure you have write permissions in this directory'
        exit(2)

    # create single main.tex
    main_tex_file = '{}/documentation.tex'.format(root_directory)
    with open(main_tex_file, 'w') as w:
        w.writelines('% this is the main documentation header\n\n')

    for list_el in [directory_flags, ignore_py_string]:
        if type(list_el) is not list:
            print "{} has to be of <type 'list'>".format(list_el)
            exit(3)

    for directory_flag in directory_flags:
        if code_directory_path is None:
            directory_search_string = directory_flag
        else:
            directory_search_string = '{}/{}'.format(code_directory_path,
                                                     directory_flag)
        if verbose:
            print 'Searching for files with: {}'.format(
                directory_search_string)

        modules = glob(directory_search_string)

        for module in modules:
            python_files = glob('{}/*py'.format(module))
            section_directory = '{}/{}'.format(root_directory_var, module)
            python_files.remove('{}/__init__.py'.format(module))
            for local_file in python_files:
                for ingore_string in ignore_py_string:
                    if ingore_string in str(local_file):
                        python_files.remove(local_file)
                        if verbose:
                            print 'Removed {}'.format(local_file)

            if len(python_files) > 0:
                mkdir(section_directory)
                if verbose:
                    print 'Created the following section directory:\n{}'.format(
                        section_directory)

                module_tex_string = str(module).replace('_', '\_')

                # write header for section_tex
                with open(main_tex_file, 'a') as w:
                    w.writelines('\section{{{}}}\n'.format(module_tex_string))
                    w.writelines('\label{{sec:{}}}\n\n'.format(module))

                if verbose:
                    print 'section: {}'.format(module_tex_string)

                # write subsections for .py files
                # along with subsubsections for each found def ()
                for python_file in python_files:
                    python_file_lines = open(python_file, 'r').readlines()
                    write_state = False
                    doc_string_counter = 0

                    subsection_string = python_file.replace(module, '')
                    subsection_string = subsection_string[1:]
                    subsection_string_label = '{}_{}'.format(
                        module, subsection_string)
                    subsection_string_label = subsection_string_label.replace(
                        '.py', '')

                    with open(main_tex_file, 'a') as a:
                        a.writelines(
                            '\subsection{{{}}}\n'.format(subsection_string))
                        a.writelines('\label{{sec:{}}}\n'.format(
                            subsection_string_label))

                    if verbose:
                        print 'added subsection: {}'.format(
                            subsection_string_label)

                    for line in python_file_lines:
                        # write state is False
                        # start writing to file if it finds a function definition
                        if 'def' in line[:4]:
                            # find the function name and use that as the subsection title/label
                            function_string = line[:line.find('(')].replace(
                                'def ', '')
                            function_tex_string = function_string.replace(
                                '_', '\_')
                            local_python_script_name = '{}/{}_{}.py'.format(
                                section_directory, function_string,
                                subsection_string_label)

                            with open(main_tex_file, 'a') as a:
                                a.writelines('\subsubsection{{{}}}\n'.format(
                                    function_tex_string))
                                a.writelines('\label{{sec:{}_{}}}\n'.format(
                                    module, function_string))
                                a.writelines(
                                    '\lstinputlisting[language=python,'
                                    'style=mystyle]{{{}/{}/{}_{}.py}}\n\n'.
                                    format(latex_root_directory, module,
                                           function_string,
                                           subsection_string_label))

                            write_state = True

                        if write_state:
                            if isfile(local_python_script_name):
                                with open(local_python_script_name, 'a') as a:
                                    a.writelines('{}'.format(line))
                            else:
                                with open(local_python_script_name, 'w') as w:
                                    w.writelines('{}'.format(line))

                        if '"""' in line:
                            doc_string_counter += 1

                        if doc_string_counter == 2:
                            write_state = False
                            doc_string_counter = 0


main(code_directory_path_var, directory_flags_var, root_directory_var,
     ignore_py_string_var)
