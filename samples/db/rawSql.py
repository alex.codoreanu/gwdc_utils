#!/usr/bin/env python
import db_utils

# This example shows how to execute raw SQL on the GWDC database


def main():
    db = db_utils.GwdcDb()

    print("** GWDC database: raw SQL example")

    with db.get_connection() as conn:
        print("List of all tables:")
        tables = conn.execute(
            "SELECT * FROM information_schema.tables WHERE table_type='base table'"
        )
        for r in tables:
            print(" * " + r['TABLE_NAME'])


main()
