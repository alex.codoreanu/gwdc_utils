#!/usr/bin/env python
import sys
import db_utils
import pandas as pd
import os

# The purpose of this example is to show how you can (a) create a database
# table from a pandas Dataframe, (b) append to that database table with another
# pandas Dataframe, and (c) extract that database table into a pandas Dataframe


def main():
    db = db_utils.GwdcDb()

    print("** GWDC database: Pandas DataTable example")

    eng = db.get_engine()

    # Load some data
    dataset1 = pd.read_csv(
        os.path.dirname(os.path.abspath(__file__)) +
        '/data/sample_triggers.csv')
    dataset1.to_sql('triggers_example', eng)
    print("Loaded %d 'triggers' into the 'triggers_example' table" %
          dataset1.size)

    # Now load some new data (with a different schema) and append that to the table
    dataset2 = pd.read_csv(
        os.path.dirname(os.path.abspath(__file__)) +
        '/data/sample_triggers_append.csv')
    dataset2.to_sql('triggers_example', eng, if_exists='append')
    print("Appended %d 'triggers' to that table" % dataset2.size)

    # Load those triggers into a new dataframe, and print out that dataframe
    final_dataset = pd.read_sql_query('SELECT * FROM triggers_example', eng)
    print("Read %d records from the database. They are:" % final_dataset.size)
    print(final_dataset)

    # Now delete the table to make it reasonably idempotent
    eng.execute('DROP TABLE triggers_example')
    print("Have dropped the 'triggers_example' table")


main()
