# Copyright (C) 2020-2021 Alex Codoreanu
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
General Notes:
    This is a testing procedure to identify if your SPIIR working environment is active and
    has access to required modules.

    Any additional modules can simply be added to the modules_to_check list.

    This procedure is interactive and does not return a bool.

Usage:
    - simply run the following import:
        - import testImports.py
    - once you've ssh'ed into OzStar you can then:
        - run  the file inside it's directory (in the directory of the file):
            python testImports.py
    - can also be used as a test of your remote interpreter from your local machine
        just run in PyCharm


"""

print "Checking imports"

modules = {}
module_fail_count = 0
modules_to_check = [
    'numpy', 'scipy', 'optparse', 'glue', 'lal', 'gstlal', 'json'
]

for local_module in modules_to_check:
    try:
        modules[local_module] = __import__(local_module)
        print "Imported: ", local_module
    except ImportError:
        print "Failed to import: ", local_module
        module_fail_count += 1

if module_fail_count < 1:
    print "All relevant modules were succesfully loaded\nYou can now run the SPIIR Pipeline"
else:
    print "FAILED to load all required modules\nYou CAN NOT succesfully run the SPIIR pipeline"
    try:
        from os import environ
        # output system variable values to help debug what the issue might be
        environment_variables = [
            'PATH', 'LD_LIBRARY_PATH', 'GST_PLUGIN_PATH', 'LAL_DATA_PATH',
            'LIBRARY_PATH', 'PKG_CONFIG_PATH', 'PYTHONPATH'
        ]

        for environment_variable in environment_variables:
            print(environment_variable)
            for ppath in str.split(str(environ[environment_variable]), ':'):
                print ppath

    except ImportError:
        print "Failed to import os module,\n check which python distribution you're pointed to"
